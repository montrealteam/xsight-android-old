package com.truefitsolutions.core;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewConfiguration;

import com.flurry.android.FlurryAgent;

import java.lang.reflect.Field;

import roboguice.activity.RoboActivity;

public class TFSActivity extends RoboActivity {

    protected int layoutResID = -1;
    protected View view;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (view != null) {
            setContentView(view);
        } else if (layoutResID != -1) {
            setContentView(layoutResID);
        }
    }

    protected int flurryResID = -1;
    @Override
    protected void onStart()
   {
        super.onStart();
        if (flurryResID != -1) {
            String flurry = getString(flurryResID);
            FlurryAgent.onStartSession(this, flurry);
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (flurryResID != -1) {
            FlurryAgent.onEndSession(this);
        }
    }

    protected void startActivity(Class<? extends Activity> startClass) {
        if (startClass != null) {
            Intent i;
            i = new Intent(this, startClass);
            startActivity(i);
        }
    }

    protected void forceOverflowMenu() {

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
