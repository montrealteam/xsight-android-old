package com.truefitsolutions.core;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewConfiguration;

import com.flurry.android.FlurryAgent;

import java.lang.reflect.Field;

import roboguice.activity.RoboFragmentActivity;

public class TFSFragmentActivity extends RoboFragmentActivity {

    protected int layoutResID = -1;
    protected View view;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (view != null) {
            setContentView(view);
        } else if (layoutResID != -1) {
            setContentView(layoutResID);
        }
    }

    protected int flurryResID = -1;
    @Override
    protected void onStart()
    {
        super.onStart();
        if (flurryResID != -1) {
            String flurry = getString(flurryResID);
            FlurryAgent.onStartSession(this, flurry);
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (flurryResID != -1) {
            FlurryAgent.onEndSession(this);
        }
    }

    protected void startActivity(Class<? extends Activity> startClass) {
        if (startClass != null) {
            Intent i;
            i = new Intent(this, startClass);
            startActivity(i);
        }
    }

    protected void startUri(String uri) {
        Uri location = Uri.parse(uri);
        if (location.getScheme() == null) {
            location = Uri.parse("http://" + uri);
        }

        final Intent intent = new Intent(Intent.ACTION_VIEW).setData(location);
        startActivity(intent);
    }

    protected void startMarketplace() {
        startUri("market://details?id=" + getPackageName());
    }

    protected void startEmail(String to, String subject) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {to});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.setType("plain/text");
        startActivity(intent);
        //startActivity(Intent.createChooser(intent, "Send your email with:"));
    }

    protected void forceOverflowMenu() {

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

