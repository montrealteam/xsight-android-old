package com.truefitsolutions.core;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import java.util.List;

import static java.lang.System.currentTimeMillis;

public class TFSStartup extends TFSActivity {

    protected int displayTime = 3000;
    protected int displayDelay = 200;
    protected boolean startupComplete;
    protected List<Integer> images;

    private Handler handler = new Handler();
    private long startTime;
    private long lastTime;
    private long now;
    private int displayedScreen;
    private int screens;

    protected void onCreate(Bundle savedInstanceState){
        view = new LinearLayout(this);
        ((LinearLayout)view).setOrientation(LinearLayout.VERTICAL);
        ((LinearLayout)view).setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = currentTimeMillis();
        lastTime = startTime;
        now = startTime;
        displayedScreen = -1;
        screens = images.size();

        handler.post(new Runnable() {
            @Override
            public void run() {
                now = currentTimeMillis();
                if ((now - startTime < displayTime) || !startupComplete) {
                    if (now - lastTime > displayDelay) {
                        displayedScreen = ((displayedScreen + 1) % screens);
                        view.setBackgroundResource(images.get(displayedScreen));
                        lastTime = now;
                    }
                    handler.postDelayed(this, displayDelay / 5);
                } else {
                    continueApp(null);
                }
            }
        });

        startupApp();
    }

    protected void startupApp() {
        startupComplete = true;
    }

    protected void continueApp(Class<? extends Activity> startClass) {
        startActivity(startClass);
        finish();
    }

 }