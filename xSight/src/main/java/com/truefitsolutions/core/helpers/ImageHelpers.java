package com.truefitsolutions.core.helpers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.HashMap;

public class ImageHelpers {

    private static HashMap<Integer, Bitmap> roundMasks = new HashMap<Integer, Bitmap>();

    public static Bitmap getRoundedCornerMask(int width, int height, int radius) {
        if (roundMasks.containsKey(width)) {
            return roundMasks.get(width);
        } else {
            Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            //init the canvas to make everything see thru
            canvas.drawARGB(255, 255, 255, 255);


            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
            paint.setColor(Color.parseColor("#00000000"));

            // draw the area you want to keep
            final float roundPx = Math.min(radius, Math.min(width/2, height/2));
            canvas.drawRoundRect(new RectF(0,0,width,height), roundPx, roundPx, paint);
            //canvas.save();
            roundMasks.put(width, output);
            return output;
        }
    }

    public static void RoundCanvas(Canvas canvas) {

        Rect r = canvas.getClipBounds();
        Bitmap roundMask = ImageHelpers.getRoundedCornerMask(r.width(), r.height(), Math.min(r.width()/2, r.height()/2));

        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        canvas.drawBitmap(roundMask, 0,0, paint);
    }
}