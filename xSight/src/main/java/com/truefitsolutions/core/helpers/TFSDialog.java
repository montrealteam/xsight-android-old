package com.truefitsolutions.core.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;

public class TFSDialog {

    public enum DialogType {
        Float,
        Text,
        Info,
        YesNo,
        Choice
    }

    public interface TFSDialogListener {
        public void onDialogClick(TFSDialog dialog, int whichButton);
    }

    private DialogType mType;
    private Context mContext;
    private TFSDialogListener mListener;
    private String mTitle;
    private String mMessage;
    private String mPositiveText;
    private String mNegativeText;
    private String mNeutralText;

    public EditText mInput;

    private AlertDialog.Builder mDialog;

    public Float mFloat = null;
    public String mText = null;

    private void BuildDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        if (mType == DialogType.YesNo) {
            mPositiveText = "Yes";
            mNegativeText = "No";
        }
        alert.setTitle(mTitle)
                .setMessage(mMessage)
                .setPositiveButton(mPositiveText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Send the positive button event back to the host activity
                        switch (mType) {
                            case Float:
                                mFloat = Float.parseFloat(mInput.getText().toString());
                            case Text:
                                mText = mInput.getText().toString();
                                break;
                            case Info:
                            case YesNo:
                                mFloat = null;
                                mText = "";
                        }
                        if (mListener != null)
                            mListener.onDialogClick(TFSDialog.this, whichButton);
                    }
                });

        if (mType != DialogType.Info)
            alert.setNegativeButton(mNegativeText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Send the negative button event back to the host activity
                    if (mListener != null)
                        mListener.onDialogClick(TFSDialog.this, whichButton);
                }
            });

        if (mType == DialogType.Choice)
            alert.setNeutralButton(mNeutralText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Send the neutral button event back to the host activity
                    if (mListener != null)
                        mListener.onDialogClick(TFSDialog.this, whichButton);
                }
            });

        if (mInput != null)
            alert.setView(mInput);

        mDialog = alert;
    }


    public TFSDialog(Context context, String title, String message, Float f, TFSDialogListener listener) {
        this(context, title, message, f, listener, "OK", "Cancel");
    }

    public TFSDialog(Context context, String title, String message, Float f, TFSDialogListener listener, String positiveText, String negativeText)
    {
        mType = DialogType.Float;
        mContext = context;
        mListener = listener;
        mTitle = title;
        mMessage = message;
        mPositiveText = positiveText;
        mNegativeText = negativeText;

        // Set an EditText view to get user input
        mInput = new EditText(mContext);

        if (f != null) {
            mInput.setText(f.toString());
        }
//        mInput.setFilters(new InputFilter[] {
//                // Maximum 2 characters.
//                //new InputFilter.LengthFilter(2),
//                // Digits only.
//                DigitsKeyListener.getInstance(),  // Not strictly needed, IMHO.
//        });

        // Digits only & use numeric soft-keyboard.
        mInput.setKeyListener(DigitsKeyListener.getInstance(false, true));
        BuildDialog();
    }

    public TFSDialog(Context context, String title, String message, String s, TFSDialogListener listener){
        this(context, title, message, s, listener, "OK", "Cancel");
    }

    public TFSDialog(Context context, String title, String message, String s, TFSDialogListener listener, String positiveText, String negativeText)
    {
        mType = DialogType.Text;
        mContext = context;
        mListener = listener;
        mTitle = title;
        mMessage = message;
        mPositiveText = positiveText;
        mNegativeText = negativeText;

        // Set an EditText view to get user input
        mInput = new EditText(mContext);

        if (s != null) {
            mInput.setText(s);
        }
        BuildDialog();
    }

    public TFSDialog(Context context, String title, String message){
        this(context, title, message, "OK", "Cancel");
    }

    public TFSDialog(Context context, String title, String message, String positiveText, String negativeText)
    {
        mType = DialogType.Info;
        mContext = context;
        mListener = null;
        mTitle = title;
        mMessage = message;
        mInput = null;
        mPositiveText = positiveText;
        mNegativeText = negativeText;

        BuildDialog();
    }

    public TFSDialog(Context context, String title, String question, TFSDialogListener listener) {
        this(context, title, question, listener, "OK", "Cancel");
    }

    public TFSDialog(Context context, String title, String question, TFSDialogListener listener, String positiveText, String negativeText)
    {
        mType = DialogType.YesNo;
        mContext = context;
        mListener = listener;
        mTitle = title;
        mMessage = question;
        mInput = null;
        mPositiveText = positiveText;
        mNegativeText = negativeText;

        BuildDialog();
    }


    public TFSDialog(Context context, String title, String message, String choice1, String choice2, String choice3, TFSDialogListener listener)
    {
        mType = DialogType.Choice;
        mContext = context;
        mListener = listener;
        mTitle = title;
        mMessage = message;

        mPositiveText = choice1;
        mNegativeText = choice2;
        mNeutralText = choice3;

        BuildDialog();
    }

    public void Show() {
        mDialog.show();
    }
}
