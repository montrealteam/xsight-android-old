package com.truefitsolutions.core.network;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class GsonRequest<T> extends Request<T> {

    //private final GsonBuilder gsonBuilder = new GsonBuilder();
    private final Gson gson = new Gson();

    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Object body;
    private final Listener<T> listener;

//    private class MyBooleanAdapter implements JsonSerializer<Boolean>, JsonDeserializer<Boolean> {
//        @Override
//        public JsonElement serialize(Boolean aBoolean, Type type, JsonSerializationContext jsonSerializationContext) {
//            return new JsonPrimitive(aBoolean ? "1" : "0");
//        }
//
//        @Override
//        public Boolean deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
//            return jsonElement.getAsBoolean();
//        }
//    }

    public GsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers, Object body, Listener<T> listener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.body = body;
        this.listener = listener;
        //gsonBuilder.registerTypeAdapter(Boolean.class, new MyBooleanAdapter());
        //gson = gsonBuilder.create();
    }

    public GsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers, Listener<T> listener, ErrorListener errorListener) {
        this(method, url, clazz, headers, null, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public String getBodyContentType() {
        return body != null ? "application/json" : super.getBodyContentType();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        byte[] b = body != null ? gson.toJson(body).getBytes() : super.getBody();
        return b;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}