package com.truefitsolutions.core.network;

public enum InterfaceState {
    Unknown,
    Setup,
    Executing,
    Complete,
    Error
}
