package com.truefitsolutions.core.network;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.inject.Inject;

import java.util.Map;

public abstract class TFSInterface<T> {
    private String url;
    private Class<T> clazz;
    protected Map<String, String> headers;
    private InterfaceState state = InterfaceState.Unknown;
    protected Object oRequest;
    protected int method = Request.Method.POST;
    protected Object oResult;
    public String error;
    private Done done = null;

    @Inject public TFSNetwork network;

    public void Init(String url, Class<T> clazz) {
        this.url = url;
        this.clazz = clazz;
        state = InterfaceState.Setup;
    }

    public void Init(String url, Class<T> clazz, int method) {
        this.Init(url, clazz);
        this.method = method;
    }

    public void InitPost(String url, Class<T> clazz) {
        this.Init(url, clazz);
        this.method = Request.Method.POST;
    }

    public void InitGet(String url, Class<T> clazz) {
        this.Init(url, clazz);
        this.method = Request.Method.GET;
    }

    protected void Execute(Done done, String urlAppend, int MY_SOCKET_TIMEOUT_MS){
        this.done = done;
        GsonRequest<T> jsObjRequest = new GsonRequest<T>(method, url + urlAppend, clazz, headers, oRequest,
                new Response.Listener<T>() {
                    @Override
                    public void onResponse(T response) {
                        TFSInterface.this.oResult = response;
                        TFSInterface.this.state = InterfaceState.Complete;
                        Done(TFSInterface.this);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse == null) {
                            TFSInterface.this.error = error.getLocalizedMessage();
                            if (TFSInterface.this.error == null) {
                                TFSInterface.this.error = error.getMessage();
                            }
                            if (TFSInterface.this.error == null) {
                                TFSInterface.this.error = error.toString();
                            }
                        } else {
                            TFSInterface.this.error = new String(error.networkResponse.data);
                        }
                        TFSInterface.this.oResult = null;
                        TFSInterface.this.state = InterfaceState.Error;
                        Done(TFSInterface.this);
                    }
                });

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        network.getRequestQueue().add(jsObjRequest);
        TFSInterface.this.state = InterfaceState.Executing;
    }

    protected void Execute(Done done){
        this.Execute(done, "", 5000);
    }

    protected void Execute(Done done, String urlAppend){
        this.Execute(done, urlAppend, 5000);
    }

    protected void Done(TFSInterface i){
        if (done != null) {
            done.Done(i);
        }
    }

    public abstract static interface Done {
        public abstract void Done(TFSInterface i);
    }

    public boolean Complete() {
        return (this.state == InterfaceState.Complete);
    }

    public boolean Error() {
        return (this.state == InterfaceState.Error);
    }

    public boolean Executing() {
        return (this.state == InterfaceState.Executing);
    }

}
