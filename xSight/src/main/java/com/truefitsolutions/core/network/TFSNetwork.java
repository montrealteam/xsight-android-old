package com.truefitsolutions.core.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

public class TFSNetwork {

    private RequestQueue mRequestQueue;
    private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
    private ImageLoader.ImageCache imageCache;
    private ImageLoader mImageLoader;

    @Inject
    public TFSNetwork(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);

        imageCache = new ImageLoader.ImageCache() {
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        };

        mImageLoader = new ImageLoader(mRequestQueue, imageCache);
    }

    public RequestQueue getRequestQueue(){
        return this.mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        return this.mImageLoader;
    }

}
