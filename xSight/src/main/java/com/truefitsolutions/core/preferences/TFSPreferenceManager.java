package com.truefitsolutions.core.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.Serializable;


@Singleton
public class TFSPreferenceManager implements Serializable {

	private static final long serialVersionUID = 516568340063702911L;
	private SharedPreferences mSharedPreferences;

    public static class Preferences {
        public final static String email = "email";
        public final static String password = "password";
        public final static String token = "token";
        public final static String userID = "userid";
    }

	@Inject
	public TFSPreferenceManager(Context context) {
		mSharedPreferences = context.getSharedPreferences("com.xsightdiscovery.xsight", Context.MODE_PRIVATE );
	}

	/*
	 * Updaters
	 */
	public boolean UpdatePreference(String key, Object value) {

		boolean success = true;
		try {
			SharedPreferences.Editor prefEditor = mSharedPreferences.edit();
			if (value == null) {
				prefEditor.remove(key);
			}

			else if (value.getClass().equals(String.class)) {
				prefEditor.putString(key, value.toString());
			}
			else if (value.getClass().equals(int.class)) {
				Log.i("PreferenceManager", String.format("Writing out int {%s:%s}", key, value.toString()));
				prefEditor.putInt(key, Integer.valueOf(value.toString()));
			}
            else if (value.getClass().equals(Float.class)) {
                Log.i("PreferenceManager", String.format("Writing out Float {%s:%s}", key, value.toString()));
                prefEditor.putFloat(key, Float.valueOf(value.toString()));
            }
			else if (value.getClass() == boolean.class) {
				prefEditor.putBoolean(key, Boolean.valueOf(value.toString()));
			}
			prefEditor.commit();
		}
		catch (Exception e) {
			success = false;
		}
		return success;

	}

	// Users

    public boolean writeJsonPreference(String key, JsonObject value) {
        boolean success = true;
        try {
            SharedPreferences.Editor prefEditor = mSharedPreferences.edit();
            prefEditor.putString(key, value.toString());
            prefEditor.commit();
        }
        catch (Exception e) {
            success = false;
        }
        return success;
    }

    public boolean writeIntPreference(String key, int value) {
        boolean success = true;
        try {
            SharedPreferences.Editor prefEditor = mSharedPreferences.edit();
            prefEditor.putInt(key, value);
            prefEditor.commit();
        }
        catch (Exception e) {
            success = false;
        }
        return success;
    }

    public boolean writeFloatPreference(String key, float value) {
        boolean success = true;
        try {
            SharedPreferences.Editor prefEditor = mSharedPreferences.edit();
            prefEditor.putFloat(key, value);
            prefEditor.commit();
        }
        catch (Exception e) {
            success = false;
        }
        return success;
    }

	public boolean writeStringPreference(String key, String value) {
		boolean success = true;
		try {
			SharedPreferences.Editor prefEditor = mSharedPreferences.edit();
			prefEditor.putString(key, value);
			prefEditor.commit();
		}
		catch (Exception e) {
			success = false;
		}
		return success;
	}

	public boolean writeBooleanPreference(String key, boolean value) {
		boolean success = true;
		try {
			SharedPreferences.Editor prefEditor = mSharedPreferences.edit();
			prefEditor.putBoolean(key, value);
			prefEditor.commit();
		}
		catch (Exception e) {
			success = false;
		}
		return success;
	}

	public boolean getBooleanPreference(String key, boolean defaultValue) {
		if (mSharedPreferences.contains(key)) {
			try {
				return mSharedPreferences.getBoolean(key, defaultValue);
			}
			catch (ClassCastException e) {
				return defaultValue;
			}
		}
		else {
			return defaultValue;
		}
	}

	public String getStringPreference(String key) {
		if (mSharedPreferences.contains(key)) {
			try {
				return mSharedPreferences.getString(key, "");
			}
			catch (ClassCastException e) {
				return "";
			}
		}
		else {
			return "";
		}
	}

	public int getIntPreference(String key) {
		return getIntPreference(key, 0);
	}
	
	public int getIntPreference(String key, int def) {
		if (mSharedPreferences.contains(key)) {
			try {
				return mSharedPreferences.getInt(key, 0);
			}
			catch (ClassCastException e) {
				return def;
			}
		}
		else {
			return def;
		}
	}

    public float getFloatPreference(String key) {
        return getFloatPreference(key, 0);
    }

    public float getFloatPreference(String key, float def) {
        if (mSharedPreferences.contains(key)) {
            try {
                return mSharedPreferences.getFloat(key, 0);
            }
            catch (ClassCastException e) {
                return def;
            }
        }
        else {
            return def;
        }
    }
}
