package com.truefitsolutions.core.social;

public class ExtendedProfile {
    // basic info
    public String accessToken;
    public String first_name;
    public String gender;
    public String id;
    public String last_name;
    public String link;
    public String locale;
    public String name;
    public int timezone;
    public String updated_time;
    public String username;
    public Boolean verified;

    // email
    public String email;

    // extended profile
    public String bio;
    public String birthday; //MM/DD/YYYY
    public FacebookValue[] checkins;
    public Education[] education;
    public FacebookValue[] events;
    public FacebookValue[] groups;
    public FacebookValue hometown;
    public FacebookValue[] likes;
    public String religion;
    public String political;
    public Work[] work;
    public FacebookValue[] books;
    public FacebookValueInsideData music;
    public FacebookValueInsideData movies;
    public FacebookValueInsideData friends;

    public class FacebookValueInsideData {
        public FacebookValue[] data;
    }

    public class FacebookValue {
        public String id;
        public String name;
    }

    public class Education {
        public FacebookValue school;
        public FacebookValue year;
        public String type;
        public String degree;
    }

    public class Work {
        public FacebookValue employer;
        public String location;
        public FacebookValue position;
        public String start_date;
        public String end_date;
    }
}

