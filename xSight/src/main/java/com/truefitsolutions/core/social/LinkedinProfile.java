package com.truefitsolutions.core.social;

public class LinkedinProfile {
    // basic info
    public String id;
    public String firstName;
    public String lastName;
    public String headline;
    public String industry;
    public String pictureUrl;

    public LinkedinDate dateOfBirth;

    public Connections connections;
    public Location location;
    public Educations educations;
    public Following following;
    public Skills skills;
    //public GroupMembership groupMemberships;
    public Positions threeCurrentPositions;
    public Positions threePastPositions;

    public class Location {
        public String name;
    }

    public class Connections {
        public Connection[] values;
    }

    public class Connection {
        public String firstName;
        public String lastName;
        public String id;
    }

    public class Educations {
        public Education[] values;
    }

    public class Education {
        public String degree;
        public String fieldOfStudy;
        public String schoolName;
        public LinkedinDate startDate;
        public LinkedinDate endDate;
        public String id;
    }

    public class Following {
        public Companies companies;
        public Industries industries;
        //public Person[] people;
        //public SpecialEdition[] specialEditions;
    }

    public class Companies {
        public Company[] values;
    }

    public class Company {
        public String id;
        public String name;
        public String title;
    }

    public class Industries {
        public Industry[] values;
    }

    public class Industry {
        public String id;
        public String name;
    }

    public class Skills {
        public Skill[] values;
    }

    public class Skill {
        public String id;
        public Name skill;
    }

    public class Name {
        public String name;
    }

    public class Positions {
        public Position[] values;
    }

    public class Position {
        public Company company;
    }

    public class LinkedinDate {
        public Integer day;
        public Integer month;
        public Integer year;
    }
}




//
//        #pragma mark - Member Url Resources
//
//        #pragma mark - Past/Current Positions
//@property (strong, nonatomic) ApiLinkedinObject* company;



//
//-(void)map:(ApiLinkedinResult*)source into:(ApiSMContainer*)target {
//
//        NSMutableArray* items = [NSMutableArray array];
//
//        if (source.headline)
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @21,
//        CLASS_KEYPATH(ApiSMItem, value): source.headline
//        }]];
//
//        if (source.industry)
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @22,
//        CLASS_KEYPATH(ApiSMItem, value): source.industry
//        }]];
//
//        if (source.location.name)
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @24,
//        CLASS_KEYPATH(ApiSMItem, value): source.location.name
//        }]];
//
//        //	[items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, type): @2,
//        //											 CLASS_KEYPATH(ApiSMItem, value):[NSString stringWithFormat:@"%@/%@/%@",source.dateOfBirth.month,source.dateOfBirth.day,source.dateOfBirth.year]
//        //											 }]];
//
//        for (ApiLinkedinObject* connection in source.connections.values) {
//        NSString* formatConnection = [NSString stringWithFormat:@"%@ %@",connection.firstName,connection.lastName];
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @17,
//        CLASS_KEYPATH(ApiSMItem, value): formatConnection,
//        CLASS_KEYPATH(ApiSMItem, customID): connection.id
//        }]];
//        }
//
//        for (ApiLinkedinObject* education in source.educations.values) {
//        NSString* formatEducation = [NSString stringWithFormat:@"%@ in %@ from %@ (%@ - %@)",education.degree,education.fieldOfStudy,
//        education.schoolName,education.startDate.year,(education.endDate.year ? education.endDate.year : @"current")];
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @20,
//        CLASS_KEYPATH(ApiSMItem, value): formatEducation,
//        CLASS_KEYPATH(ApiSMItem, customID): education.id
//        }]];
//        }
//
//        for (ApiLinkedinObject* following in source.following.companies.values) {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @18,
//        CLASS_KEYPATH(ApiSMItem, value): following.name,
//        CLASS_KEYPATH(ApiSMItem, customID): following.id
//        }]];
//        }
//
//        for (ApiLinkedinObject* membership in source.groupMemberships.values) {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @19,
//        CLASS_KEYPATH(ApiSMItem, value): membership.group.name,
//        CLASS_KEYPATH(ApiSMItem, customID): membership.group.id
//        }]];
//        }
//
//        for (ApiLinkedinObject* resource in source.memberUrlResources.values) {
//        //		[items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @25,
//        //												 CLASS_KEYPATH(ApiSMItem, value): membership.group.name,
//        //												 CLASS_KEYPATH(ApiSMItem, customID): membership.group.id}]];
//        }
//
//        for (ApiLinkedinObject* recommendation in source.recommendationsReceived.values) {
//        //		[items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): ,
//        //												 CLASS_KEYPATH(ApiSMItem, value): recommendation.name,
//        //												 CLASS_KEYPATH(ApiSMItem, customID): recommendation.id
//        //												 }]];
//        }
//
//        for (ApiLinkedinObject* skill in source.skills.values) {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @26,
//        CLASS_KEYPATH(ApiSMItem, value): skill.skill.name,
//        CLASS_KEYPATH(ApiSMItem, customID): skill.id
//        }]];
//        }
//
//        for (ApiLinkedinObject* currentPosition in source.threeCurrentPositions.values) {
//        if (currentPosition.company.id) {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @27,
//        CLASS_KEYPATH(ApiSMItem, value): currentPosition.company.name,
//        CLASS_KEYPATH(ApiSMItem, customID): currentPosition.company.id
//        }]];
//        } else {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @27,
//        CLASS_KEYPATH(ApiSMItem, value): currentPosition.company.name
//        }]];
//        }
//        }
//
//        for (ApiLinkedinObject* pastPosition in source.threePastPositions.values) {
//        if (pastPosition.company.id) {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @28,
//        CLASS_KEYPATH(ApiSMItem, value): pastPosition.company.name,
//        CLASS_KEYPATH(ApiSMItem, customID): pastPosition.company.id
//        }]];
//        } else {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @28,
//        CLASS_KEYPATH(ApiSMItem, value): pastPosition.company.name
//        }]];
//        }
//
//        }
//
//        if (source.pictureUrl)
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @29,
//        CLASS_KEYPATH(ApiSMItem, value): source.pictureUrl
//        }]];


