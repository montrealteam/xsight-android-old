package com.xsightdiscovery.xsight;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.api.GetChat;
import com.xsightdiscovery.xsight.api.MarkChatsRead;
import com.xsightdiscovery.xsight.api.SendChat;
import com.xsightdiscovery.xsight.api.Models.User;

import roboguice.inject.InjectView;

public class ChatActivity extends xSightActivity implements View.OnClickListener, TFSInterface.Done {

    @Inject GetChat getChat;
    @Inject SendChat sendChat;
    @Inject MarkChatsRead markChatsRead;

    @InjectView(R.id.chat_list) ListView list;
    @InjectView(R.id.chat_text) EditText chat_text;
    @InjectView(R.id.chat_send) TextView chat_send;
    @InjectView(R.id.chat_loading) View chat_loading;

    ChatAdapter adapter;
    User user;
    int unreadChatCount = 0;

    private Handler handler = new Handler();
    private Boolean checking = false;
    private Boolean continueChecking = true;

    private static final String AD_UNIT_ID = "ca-app-pub-2067666827960195/9514744060";
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Intent i = getIntent();
        user = i.getParcelableExtra(Global.USER);

        chat_send.setOnClickListener(this);

        adapter = new ChatAdapter(this, null, user.CurrentPicUrl());
        list.setAdapter(adapter);

        //set up and load ads
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(AD_UNIT_ID);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("4F2BEB2641859EB92A169B74B67073A9")  //Vasanth test phone 1 - Samsung Galaxy S4
                .addTestDevice("6C2B2A27BEDE0331E7727679D82CF095")  //Vasanth test phone 2 - Samsung Galaxy S4
                .addTestDevice("658DCA7AB800BD9D2D1E16BF66366800")  //Vasanth test tablet 7 inch
                .addTestDevice("A4A2F220A01724FB3F8965B38A114D3B")  //Vasanth test tablet 10 inch
                .build();

        interstitialAd.loadAd(adRequest);     //begin loading
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getChat.Init(user.ID);
        markChatsRead.Init(user.ID);

        continueChecking = true;
        checkForNewChat();

        chat_loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        continueChecking = false;

        displayInterstitialAd();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.chat_send) {
            String chatText = chat_text.getText().toString();

            if(chatText.length() == 0)
                Toast.makeText(this, "Chat must include at least one character", Toast.LENGTH_LONG).show();
            else if(chatText.length() > 1000)       //max chat length on server is 1000 characters
                Toast.makeText(this, "Chat must be shorter than 1000 characters", Toast.LENGTH_LONG).show();
            else {
                sendChat.Init(user.ID, chat_text.getText().toString());
                sendChat.Execute(this);
                chat_loading.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void Done(TFSInterface i) {
        chat_loading.setVisibility(View.GONE);

        if (i instanceof GetChat) {
            if (i.Complete()) {
                // Fill in the list view
                adapter.setItems(getChat.chats);
                checking = false;

                //mark chats as read
                markChatsRead.Execute(this);

            } else {
                Toast.makeText(this, "Unable to load chat messages.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }

        if (i instanceof SendChat) {
            if (i.Complete()) {
                chat_text.setText("");
                getChat.Execute(this);
            } else {
                Toast.makeText(this, "Unable to send chat message.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }

        if(i instanceof MarkChatsRead) {
            if (i.Complete()) {
                unreadChatCount = Integer.parseInt(markChatsRead.unreadChat.unreadChats);   //TODO: in the future if there is an app-level count of unread items, this is what we would use
            } else {
                Toast.makeText(this, "Unable to mark chats as read.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkForNewChat() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!checking) {
                    checking = true;
                    chat_loading.setVisibility(View.VISIBLE);
                    getChat.Execute(ChatActivity.this);
                }
                if (continueChecking) {
                    handler.postDelayed(this, 10000);
                }
            }
        });
    }

    public void displayInterstitialAd() {
        if (interstitialAd.isLoaded())
            interstitialAd.show();
    }
}