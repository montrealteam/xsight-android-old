package com.xsightdiscovery.xsight;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSNetwork;
import com.xsightdiscovery.xsight.api.Models.Chat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import roboguice.RoboGuice;

public class ChatAdapter extends BaseAdapter {

    @Inject Global global;
    @Inject TFSNetwork network;
    private Context c;
    private Chat[] items;
    private String picture;

    public ChatAdapter(Context c, Chat[] items, String picture){
        this.c = c;
        this.items = items;
        this.picture = picture;
        RoboGuice.getInjector(c).injectMembers(this);
    }

    public void setItems(Chat[] items) {
        ArrayList<Chat> itemsList = new ArrayList<Chat>(Arrays.asList(items));
        Collections.sort(itemsList);

        Chat[] sortedArray = new Chat[itemsList.size()];
        sortedArray = itemsList.toArray(sortedArray);

        this.items = sortedArray;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (items == null) ? 0 : items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        Chat x = items[position];
        return x.ID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(c, R.layout.view_chat_list, null);
            if (android.os.Build.VERSION.SDK_INT >= 11)
            {
                convertView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
            holder = new ViewHolder();

            holder.otherImage = (NetworkImageView)convertView.findViewById(R.id.other_chat_image);
            holder.otherImage.setDefaultImageResId(R.drawable.default_profile_pic);
            holder.otherImage.setErrorImageResId(R.drawable.default_profile_pic);
            holder.otherImage.setImageUrl(picture, network.getImageLoader());

            holder.message = (TextView)convertView.findViewById(R.id.chat_text);

            holder.myImage = (NetworkImageView)convertView.findViewById(R.id.my_chat_image);
            holder.myImage.setDefaultImageResId(R.drawable.default_profile_pic);
            holder.myImage.setErrorImageResId(R.drawable.default_profile_pic);
            holder.myImage.setImageUrl(global.user.CurrentPicUrl(), network.getImageLoader());

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        Chat chat = items[position];

        holder.message.setText(chat.Message);
        if (chat.FromUserID.equals(global.user.ID)) {
            holder.otherImage.setVisibility(View.GONE);
            holder.message.setGravity(Gravity.RIGHT);
            holder.myImage.setVisibility(View.VISIBLE);

        } else {
            holder.otherImage.setVisibility(View.VISIBLE);
            holder.message.setGravity(Gravity.LEFT);
            holder.myImage.setVisibility(View.GONE);
        }

        return convertView;
    }


    /** Helper classes **/
    /*private view holder class*/
    private class ViewHolder {
        NetworkImageView otherImage;
        TextView message;
        NetworkImageView myImage;
    }
}
