package com.xsightdiscovery.xsight;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rbrinkman on 6/17/14.
 *
 * Class name must match the name of the table in the Azure database
 */
public class Devices {

    private String token;

    private final int type = 2;     //1 is iOS, 2 is Android

    public String getToken() {
        return token;
    }

    public final void setToken(String t) {
        token = t;
    }

    public int getType() {
        return type;
    }
}
