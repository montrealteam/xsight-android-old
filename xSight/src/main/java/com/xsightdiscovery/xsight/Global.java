package com.xsightdiscovery.xsight;

import com.google.inject.Singleton;
import com.xsightdiscovery.xsight.api.GetSocialMediaPrefs;
import com.xsightdiscovery.xsight.api.Models.DisplayProfile;
import com.xsightdiscovery.xsight.api.Models.User;

@Singleton
public class Global {
    public User user;
    public DisplayProfile displayProfile;

    public GetSocialMediaPrefs.SocialMediaPrefs[] broadcastPrefs;

    public enum Preferences {
        range
    }

    public final static String USER = "com.xsight.USER";
    public final static String PUSH_NOTIFICATIONS_SENDER_ID = "472177365901";   //project number assigned by Google to the XSight app

    public User targetUser;
    public int mainActivityView;

    public void setSocialMediaPrefs(int socialMediumID, boolean professional, boolean personal) {
        if (broadcastPrefs != null) {
            for (GetSocialMediaPrefs.SocialMediaPrefs smp : broadcastPrefs ) {
                if (smp.SocialMediumID == socialMediumID) {
                    smp.PersonalProfile = personal;
                    smp.ProfessionalProfile = professional;
                }
            }
        }
    }

}
