package com.xsightdiscovery.xsight;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.inject.Inject;
import com.truefitsolutions.core.TFSActivity;
import com.truefitsolutions.core.helpers.TFSDialog;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.xsightdiscovery.xsight.api.ForgotPassword;
import com.xsightdiscovery.xsight.api.Login;

public class LoginActivity extends TFSActivity implements View.OnClickListener, TFSInterface.Done, TFSDialog.TFSDialogListener {

    @Inject TFSPreferenceManager pm;
    @Inject Login login;
    @Inject ForgotPassword forgotPassword;

    EditText email;
    EditText password;

    TFSDialog forgotPasswordDialog;
    String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        flurryResID = R.string.flurry_api_key;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.loginSignIn).setOnClickListener(this);
        findViewById(R.id.gotoCreateAccount).setOnClickListener(this);
        findViewById(R.id.loginForgotPassword).setOnClickListener(this);

        email = (EditText)findViewById(R.id.email);
        email.setText(pm.getStringPreference(TFSPreferenceManager.Preferences.email));
        password = (EditText)findViewById(R.id.password);

        forgotPasswordDialog = new TFSDialog(this, "Forgot Password", "Enter your account's email", email.getText().toString(), this,  "Send New Password", "Cancel"
        );
    }

    @Override
     public void onClick(View v) {

        switch(v.getId()) {
            case R.id.loginSignIn:
                pm.writeStringPreference(TFSPreferenceManager.Preferences.email, email.getText().toString());
                login.Init(email.getText().toString(), password.getText().toString(), LoginActivity.this);
                login.Execute(this);
                break;
            case R.id.gotoCreateAccount:
                pm.writeStringPreference(TFSPreferenceManager.Preferences.email, email.getText().toString());
                startActivity(RegisterActivity.class);
                break;
            case R.id.loginForgotPassword:
                forgotPasswordDialog.Show();
                break;
        }
    }

    @Override
    public void Done(TFSInterface i) {
        if(i instanceof ForgotPassword) {
            if (forgotPassword.Complete()) {
                Toast.makeText(this, "New password sent", Toast.LENGTH_LONG).show();

                Intent intent;
                intent = new Intent(this, ResetPasswordActivity.class);
                intent.putExtra("userEmail", userEmail);
                startActivity(intent);
            }
            else
                Toast.makeText(this, "Unable to reset, please check email address", Toast.LENGTH_LONG).show();
        }
        else if (i instanceof Login) {
            if (login.Complete()) {
                startActivity(MainActivity.class);
                finish();
            } else {
                password.setText("");
                Toast.makeText(this, login.error, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onDialogClick(TFSDialog dialog, int whichButton) {
        if (whichButton == DialogInterface.BUTTON_POSITIVE) {
            if(dialog.mText.length() > 4) {
                userEmail = dialog.mText.toString();
                forgotPassword.Init(userEmail);
                forgotPassword.Execute(this);
            }
            else
                Toast.makeText(this, "Please enter your email address", Toast.LENGTH_LONG).show();
        }
    }
}
