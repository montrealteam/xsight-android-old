package com.xsightdiscovery.xsight;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.truefitsolutions.core.helpers.TFSDialog;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.truefitsolutions.core.social.DialogListener;
import com.truefitsolutions.core.social.SocialAuthAdapter;
import com.truefitsolutions.core.social.SocialAuthError;
import com.truefitsolutions.core.social.SocialAuthListener;
import com.xsightdiscovery.xsight.api.GetSocialMediaPrefs;
import com.xsightdiscovery.xsight.api.GetUsers;
import com.xsightdiscovery.xsight.api.Models.DisplayProfile;
import com.xsightdiscovery.xsight.api.ProfileService;
import com.xsightdiscovery.xsight.api.RegisterDevice;
import com.xsightdiscovery.xsight.api.SetMyInfo;
import com.xsightdiscovery.xsight.api.SetSocialMediaProfile;
import com.xsightdiscovery.xsight.api.Models.User;
import com.xsightdiscovery.xsight.api.XSightSocialMediaAdapter;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.microsoft.windowsazure.mobileservices.*;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import roboguice.inject.InjectView;

public class MainActivity extends xSightLocationActivity implements View.OnClickListener, TFSInterface.Done,
        ProfileService.ProfileServiceDelegate, XSightSocialMediaAdapter.SocialMediaRetrievalDelegate {

    @Inject TFSPreferenceManager preferenceManager;
    @Inject Global global;
    @Inject GetUsers getUsers;
    @Inject SetMyInfo setMyInfo;

    @Inject public RegisterDevice registerDevice;

    @InjectView(R.id.mode_text) TextView mode;

    @Inject ProfileService profileService;
    @Inject SetSocialMediaProfile setSocialMediaProfile;

    private static MobileServiceClient mClient;
    private String mHandle;

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    XSightSocialMediaAdapter facebookAdapter;
    XSightSocialMediaAdapter twitterAdapter;

    private static XSightSocialMediaAdapter socialAdapter;

    private Boolean hasUpdatedSocialMedia;
    private Boolean hasLatestFacebook;
    private Boolean hasLatestTwitter;
    private Boolean hasLatestLinkedIn;

    float mapRange;
    static float KM_CONVERSION = 1.60934f;

    Timer userListTimer;
    Timer pollSocialMediaConfigurationTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hasUpdatedSocialMedia = false;
        hasLatestFacebook = false;
        hasLatestTwitter = false;
        hasLatestLinkedIn = false;

        facebookAdapter = new XSightSocialMediaAdapter(new SocialResponseListener() {
            @Override
            public void onComplete(Bundle values) {
                facebookAdapter.updateStatus(
                        "I am using XSight Mobile App to discover new connections and friends! Please join me by downloading the app via iTunes or Google Play and discover who you want to know around you!",
                        new SocialMessageListener(), false);
            }

        }, global);
        facebookAdapter.addProvider(SocialAuthAdapter.Provider.FACEBOOK, R.drawable.facebook);

        twitterAdapter = new XSightSocialMediaAdapter(new SocialResponseListener() {
            @Override
            public void onComplete(Bundle values) {
                twitterAdapter.updateStatus(
                        "I am using XSight Mobile App to discover new connections and friends! Join me by downloading the app via iTunes or Google Play!",
                        new SocialMessageListener(), false);
            }

        }, global);
        twitterAdapter.addProvider(SocialAuthAdapter.Provider.TWITTER, R.drawable.twitter);
        twitterAdapter.addCallBack(SocialAuthAdapter.Provider.TWITTER, "http://www.xsightdiscovery.com");



        // Create the adapter that will return a fragment for each of the two primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mapRange = preferenceManager.getFloatPreference(Global.Preferences.range.toString(), 2);

        try {
            mClient = new MobileServiceClient(
                    "https://xsight.azure-mobile.net/",
                    "jRUtuYFLqnLtCxQcpBCevGpDRnLBkF32",
                    this
            );
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        socialAdapter = new XSightSocialMediaAdapter(this, global);

        NotificationsManager.handleNotifications(this, Global.PUSH_NOTIFICATIONS_SENDER_ID, PushNotificationHandler.class);
    }

    @Override
    public void onResume() {
        super.onResume();

        //fetch primary user's profile to use in comparisons
        profileService.fetchProfileForID(this, global.user);

        setModeInfo();

        getUsers.setmContext(this);
        getUsers.Init(mapRange * KM_CONVERSION);

        userListTimer = new Timer();
        userListTimer.schedule(new UserListTimerTask(this), new Date(), 10000);     //update every 10 seconds

        mViewPager.setCurrentItem(global.mainActivityView);

        startUpdates();

        //check if the user has associated social media yet, if not, prompt them to do so
        if(!preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.FACEBOOK.toString(), false) &&
                !preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.TWITTER.toString(), false) &&
                !preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.LINKEDIN.toString(), false)) {

            startActivity(SettingsActivity.class);
        }
        else if(!hasUpdatedSocialMedia) {
            hasUpdatedSocialMedia = true;
            String provider = configureSocialAdapter();

            pollSocialMediaConfigurationTimer = new Timer();
            pollSocialMediaConfigurationTimer.schedule(new PollSocialMediaTimerTask(this, provider), new Date(), 10000);     //update every 10 seconds
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        userListTimer.cancel();
        userListTimer.purge();
        userListTimer = null;
    }

    /**
     * Returns the client reference
     */
    public static MobileServiceClient getClient() {
        return mClient;
    }

    public String getHandle() {
        return mHandle;
    }

    public final void setHandle(String handle) {
        mHandle = handle;
    }

    private void setModeInfo() {

        String modeText = global.user.ProfessionalMode ? getResources().getString(R.string.title_section2) : getResources().getString(R.string.title_section1);

        mode.setText(modeText);
        ab_facebook.setVisibility(View.GONE);
        ab_linkedIn.setVisibility(View.GONE);
        ab_twitter.setVisibility(View.GONE);
        if (global.broadcastPrefs != null) {
            for (GetSocialMediaPrefs.SocialMediaPrefs smp : global.broadcastPrefs ) {
                if ((smp.PersonalProfile && !global.user.ProfessionalMode) || (smp.ProfessionalProfile && global.user.ProfessionalMode)) {
                    switch (smp.SocialMediumID) {
                        case 1:
                            ab_facebook.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            ab_twitter.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            ab_linkedIn.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Double lat = location.getLatitude();
        Double lng = location.getLongitude();

        if (!lat.equals(global.user.Latitude) || !lng.equals(global.user.Longitude)) {
            global.user.Latitude = lat;
            global.user.Longitude = lng;

            // GetUsers set current users location so don't need two calls.
            getUsers.Init(mapRange * KM_CONVERSION);
            getUsers.Execute(this);
        }
    }

    public void activate(View v){
        ViewGroup vg = (ViewGroup)v.getParent();

        for (int i = 0; i < vg.getChildCount(); i++) {
            View ov = vg.getChildAt(i);
            if (ov.getClass() == ImageButton.class) {
                ov.setActivated(false);
            }
        }
        v.setActivated(true);
    }

    @Override
    public void onClick(View v) {

    }

    public abstract static interface ShowUsers {
        public abstract void ShowUsers(User[] users);
    }

    public Fragment findFragmentByPosition(int position) {
        return getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + mViewPager.getId() + ":" + position);
    }

    @Override
    public void Done(TFSInterface i) {

        if (i instanceof GetUsers) {
            if (i.Complete()) {
                // Fill in the views
                for (int x = 0; x < mViewPager.getChildCount(); x++) {
                    ShowUsers f = (ShowUsers)findFragmentByPosition(x);
                    if (f != null) {
                        f.ShowUsers(getUsers.users);
                    }
                }
            } else {
                Toast.makeText(this, "Unable to load users.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }

        if (i instanceof SetMyInfo) {
            if (i.Complete()) {
                // Fill in the mode
                setModeInfo();
            } else {
                Toast.makeText(this, "Unable to save user settings.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_switch_mode:
                global.user.ProfessionalMode = !global.user.ProfessionalMode;
                setMyInfo.Init();
                setMyInfo.Execute(this);
                return true;
            case R.id.action_show_my_profile:
                Intent intent = new Intent(this, ProfileActivity.class);
                intent.putExtra(Global.USER, global.user);
                startActivity(intent);
                return true;
            case R.id.action_adjust_map_range:
                Float miles = preferenceManager.getFloatPreference(Global.Preferences.range.toString(), 2);
                TFSDialog d = new TFSDialog(this, "Map Range", "Enter the range in miles you would like to see other users.\n\n(The default is 2 miles.)", miles,
                        new TFSDialog.TFSDialogListener() {
                            public void onDialogClick(TFSDialog dialog, int whichButton) {

                                if (whichButton == DialogInterface.BUTTON_POSITIVE) {
                                    float newRange = dialog.mFloat;
                                    newRange = (newRange <= 0) ? 0.1f : newRange;
                                    //newRange = (newRange > 10) ? 10 : newRange;

                                    preferenceManager.writeFloatPreference(Global.Preferences.range.toString(), newRange);
                                    mapRange = preferenceManager.getFloatPreference(Global.Preferences.range.toString(), 2);

                                    getUsers.Init(preferenceManager.getFloatPreference(Global.Preferences.range.toString(), mapRange * KM_CONVERSION));
                                    getUsers.Execute(MainActivity.this);
                                }
                            }
                        });
                d.mInput.setTextColor(getResources().getColor(android.R.color.black));
                d.Show();
                return true;
            case R.id.action_settings:
                startActivity(SettingsActivity.class);
                return true;
            case R.id.action_share:

                d = new TFSDialog(this, "Share XSight", "Would you like to share XSight with your friends on social media?",
                        new TFSDialog.TFSDialogListener() {
                            public void onDialogClick(TFSDialog dialog, int whichButton) {
                                if (whichButton == DialogInterface.BUTTON_POSITIVE) {
                                    if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.FACEBOOK.toString(), false))
                                        facebookAdapter.authorize(MainActivity.this, SocialAuthAdapter.Provider.FACEBOOK);

                                    if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.TWITTER.toString(), false))
                                        twitterAdapter.authorize(MainActivity.this, SocialAuthAdapter.Provider.TWITTER);
                                }
                            }
                        });
                d.Show();
                return true;

            case R.id.action_terms:
                startUri("http://xsightdiscovery.com/privacy.html");
                return true;
            case R.id.action_about:
                String verText;
                try {
                    verText = "XSight Discovery: Version " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    verText = "XSight Discovery: Version " + "Unknown";
                }
                d = new TFSDialog(this, "About", verText);
                d.Show();
                return true;
            case R.id.action_rate:
                startMarketplace();
                return true;
            case R.id.action_feedback:
                startEmail("feedback@xsightmobile.com", "Feedback");
                return true;
            case R.id.action_sign_out:
                global.user.Active = false;
                setMyInfo.Init();
                setMyInfo.Execute(this);

                preferenceManager.writeStringPreference(TFSPreferenceManager.Preferences.password, null);
                startActivity(LoginActivity.class);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
     }

    @Override
    public void profileReceived(Boolean facebookFlag, Boolean twitterFlag, Boolean linkedInFlag) {
        //do nothing
    }

    private String configureSocialAdapter() {
        if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.FACEBOOK.toString(), false) == false)
            hasLatestFacebook = true;   //user doesn't have facebook
        if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.TWITTER.toString(), false) == false)
            hasLatestTwitter = true;    //user doesn't have twitter
        if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.LINKEDIN.toString(), false) == false)
            hasLatestLinkedIn = true;   //user doesn't have linkedin

        if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.FACEBOOK.toString(), false) && !hasLatestFacebook) {
            socialAdapter.authorize(MainActivity.this, SocialAuthAdapter.Provider.FACEBOOK);
            hasLatestFacebook = true;
            return SocialAuthAdapter.Provider.FACEBOOK.toString();
        }
        else if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.TWITTER.toString(), false) && !hasLatestTwitter) {
            socialAdapter.authorize(MainActivity.this, SocialAuthAdapter.Provider.TWITTER);
            hasLatestTwitter = true;
            return SocialAuthAdapter.Provider.TWITTER.toString();
        }
        else if(preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.LINKEDIN.toString(), false) && !hasLatestLinkedIn) {
            socialAdapter.authorize(MainActivity.this, SocialAuthAdapter.Provider.LINKEDIN);
            hasLatestLinkedIn = true;
            return SocialAuthAdapter.Provider.LINKEDIN.toString();
        }

        return null;
    }

    @Override
    public void profileSorted(DisplayProfile dp) {
        global.displayProfile = dp;
    }


    /** SocialMediaRetrievalDelegate **/
    public void socialMediaDataRetrieved(List<SetSocialMediaProfile.Request> profile) {
        if (profile != null) {
            setSocialMediaProfile.Init(profile.toArray(new SetSocialMediaProfile.Request[profile.size()]));
            setSocialMediaProfile.Execute(MainActivity.this);

            setMyInfo.Init();
            setMyInfo.Execute(MainActivity.this);
        }

        if(!hasLatestFacebook || !hasLatestTwitter || !hasLatestLinkedIn) {
            String provider = configureSocialAdapter();

            pollSocialMediaConfigurationTimer.cancel();
            pollSocialMediaConfigurationTimer.purge();

            pollSocialMediaConfigurationTimer = new Timer();
            pollSocialMediaConfigurationTimer.schedule(new PollSocialMediaTimerTask(this, provider), new Date(), 10000);     //update every 10 seconds
        }
    }


    /** Helper classes **/

    private class SocialResponseListener implements DialogListener {

        public void onComplete(Bundle values) {
            //override as needed
        }

        public void onError(SocialAuthError error) {
            Log.d("ShareButton", "Error " + error);
        }

        public void onCancel() {
            Log.d("ShareButton" , "Cancelled");
        }

        public void onBack() {
            Log.d("ShareButton" , "Cancelled");
        }

        public void onAllDone(List<SetSocialMediaProfile.Request> profile) {
            Log.d("ShareButton" , "Done");
        }

        protected final class SocialMessageListener implements SocialAuthListener<Integer> {

            public void onExecute(String s, Integer t) {
                Integer status = t;
                if (status.intValue() == 200 || status.intValue() == 201 ||status.intValue() == 204)
                    Toast.makeText(MainActivity.this, "Message posted",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MainActivity.this, "Message not posted",Toast.LENGTH_LONG).show();
            }

            public void onError(SocialAuthError e) {
                Log.d("ShareButton", "Error " + e);
            }
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /*
        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    fragment = MainLocationFragment.newInstance(getUsers.users);
                    break;
                default:
                    fragment = MainListFragment.newInstance(getUsers.users);
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }*/

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = MainListFragment.newInstance(getUsers.users);

            return fragment;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    class UserListTimerTask extends TimerTask {
        MainActivity mainActivityPtr;

        UserListTimerTask(MainActivity mainActivity) {
            mainActivityPtr = mainActivity;
        }

        public void run() {
            getUsers.Execute(mainActivityPtr);
        }
    }

    class PollSocialMediaTimerTask extends TimerTask {
        MainActivity mainActivityPtr;
        String currentProvider;

        PollSocialMediaTimerTask(MainActivity mainActivity, String provider) {
            mainActivityPtr = mainActivity;
            currentProvider = provider;
        }

        public void run() {
            if(socialAdapter.getProvider(currentProvider) != null) {
                socialAdapter.GetAndSaveSocialMediaProfile();   //pull latest info from social media

                pollSocialMediaConfigurationTimer.cancel();
                pollSocialMediaConfigurationTimer.purge();
            }
        }
    }
}
