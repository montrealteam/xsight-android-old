package com.xsightdiscovery.xsight;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.inject.Inject;
import com.truefitsolutions.core.TFSFragment;
import com.xsightdiscovery.xsight.api.Models.User;

import roboguice.inject.InjectView;

public class MainListFragment extends TFSFragment implements MainActivity.ShowUsers, AdapterView.OnItemClickListener {

    private static final String USERS_KEY = "users_key";
    @Inject Global global;
    @InjectView(R.id.user_list) ListView list;
    private User[] users;
    UserListAdapter adapter;


    public static MainListFragment newInstance(User[] users) {
        MainListFragment fragment = new MainListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USERS_KEY, users);
        fragment.setArguments(bundle);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_list, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new UserListAdapter(this.getActivity(), null);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
        ShowUsers((User[]) getArguments().getSerializable(USERS_KEY));
    }

    @Override
    public void ShowUsers(User[] users) {
        this.users = users;
        if (users != null){
            adapter.setItems(users);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = users[position];
        if (user != null){
            Intent intent = new Intent(this.getActivity(), ProfileActivity.class);
            intent.putExtra(global.USER, user);
            this.getActivity().startActivity(intent);
        }
    }
}
