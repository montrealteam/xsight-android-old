package com.xsightdiscovery.xsight;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.inject.Inject;
import com.truefitsolutions.core.TFSFragment;
import com.xsightdiscovery.xsight.api.Models.User;

import java.util.HashMap;

public class MainLocationFragment extends TFSFragment
        implements MainActivity.ShowUsers, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMyLocationButtonClickListener {

    private static final String USERS_KEY = "users_key";
    @Inject Global global;
    private GoogleMap map;
    private BitmapDescriptor marker;
    private HashMap<String, User> markerMap = new HashMap<String, User>();
    private Boolean zoomedAlready = false;

    public static MainLocationFragment newInstance(User[] users) {
        MainLocationFragment fragment = new MainLocationFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USERS_KEY, users);
        fragment.setArguments(bundle);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_location, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get a handle to the Map Fragment

        map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        marker = BitmapDescriptorFactory.fromResource(R.drawable.ic_active_map);

        ShowUsers((User[]) getArguments().getSerializable(USERS_KEY));
    }

    @Override
    public void ShowUsers(User[] users) {

        if (users != null && map != null) {

            map.setMyLocationEnabled(true);
            map.setOnInfoWindowClickListener(this);
            map.clear();
            markerMap.clear();

            for (int i=0; i<users.length; i++) {

                //make sure the user has allowed their location to be seen on the map
                if(users[i].IsMapVisible) {

                    LatLng latLng;
                    if (users[i].Latitude == null || users[i].Longitude == null)
                        latLng = new LatLng(0.0, 0.0);
                    else
                        latLng = new LatLng(users[i].Latitude, users[i].Longitude);

                    if (!users[i].ID.equals(global.user.ID)) {
                        String title;
                        title = users[i].FirstName + " " + users[i].LastName;

                        MarkerOptions mo = new MarkerOptions()
                                .title(title)
                                .icon(marker)
                                .position(latLng);

                        Marker m = map.addMarker(mo);
                        markerMap.put(m.getId(), users[i]);

                        if (global.targetUser != null && users[i].ID.equals(global.targetUser.ID)) {
                            m.showInfoWindow();
                        }
                    }
                }
            }

            LatLng latLng;
            if (global.targetUser != null) {
                latLng = new LatLng(global.targetUser.Latitude, global.targetUser.Longitude);
            } else {
                latLng = new LatLng(global.user.Latitude, global.user.Longitude);
            }
            if (!zoomedAlready) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16), 2000, null);
            } else {
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng), 2000, null);
            }
            zoomedAlready = true;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        User user = markerMap.get(marker.getId());
        if (user != null){
            Intent intent = new Intent(this.getActivity(), ProfileActivity.class);
            intent.putExtra(global.USER, user);
            this.getActivity().startActivity(intent);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        // clear out target user if they ask to focus on themselves again
        global.targetUser = null;
        return false;
    }
}
