package com.xsightdiscovery.xsight;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.truefitsolutions.core.social.SocialAuthAdapter.Provider;
import com.xsightdiscovery.xsight.api.GetSocialMediaDataPrefs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MediaDataAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {

    private static Set<String> linkedInForbiddenPrefs = new HashSet<>();

    static {
        String[] forbiddenPrefs = {"Education", "Connections", "Group Membership", "Following Company", "Member URL Resource", "Past Position", "Skills", "Industry", "Current Position"};
        for(String pref: forbiddenPrefs)
        linkedInForbiddenPrefs.add(pref);
    }

    private Context c;
    private GetSocialMediaDataPrefs.SocialMediaDataPrefs[] items;
    private int socialMediumID;

    public MediaDataAdapter(Context c, GetSocialMediaDataPrefs.SocialMediaDataPrefs[] items, int socialMediumID){
        this.c = c;
        this.items = items;
        this.socialMediumID = socialMediumID;
    }

    public void setSocialMediumID(int socialMediumID){
        this.socialMediumID = socialMediumID;
        notifyDataSetChanged();
    }

    public int getSocialMediumID(){
        return this.socialMediumID;
    }

    public Provider getSocialMediumProvider(){
        switch(this.socialMediumID) {
            case 1:
                return Provider.FACEBOOK;
            case 2:
                return Provider.TWITTER;
            case 3:
                return Provider.LINKEDIN;
            default:
                return null;
        }
    }


    public void setItems(GetSocialMediaDataPrefs.SocialMediaDataPrefs[] items){
        List<GetSocialMediaDataPrefs.SocialMediaDataPrefs> prefs = new ArrayList<>();
        for(GetSocialMediaDataPrefs.SocialMediaDataPrefs pref : items) {
            if (!linkedInForbiddenPrefs.contains(pref.Type.Name)) {
                prefs.add(pref);
            }
        }
        this.items = prefs.toArray(new GetSocialMediaDataPrefs.SocialMediaDataPrefs[prefs.size()]);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (items == null) ? 0 : items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        GetSocialMediaDataPrefs.SocialMediaDataPrefs x = items[position];
        return x.ID;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView name;
        Switch toggle;
        GetSocialMediaDataPrefs.SocialMediaDataPrefs pref;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(c, R.layout.view_media_switch, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.data_name);
            holder.toggle = (Switch)convertView.findViewById(R.id.data_switch);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.pref = items[position];

        holder.name.setText(items[position].Type.Name);
        holder.toggle.setChecked(items[position].Share == 1);
        holder.toggle.setOnCheckedChangeListener(this);

        if (items[position].Type.SocialMediumID == socialMediumID) {
            convertView.setVisibility(View.VISIBLE);
            holder.name.setVisibility(View.VISIBLE);
            holder.toggle.setVisibility(View.VISIBLE);
        } else {
            convertView.setVisibility(View.GONE);
            holder.name.setVisibility(View.GONE);
            holder.toggle.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        ViewHolder holder = (ViewHolder)((View)(buttonView.getParent())).getTag();
        holder.pref.Share = isChecked ? 1 : 0;
    }
}
