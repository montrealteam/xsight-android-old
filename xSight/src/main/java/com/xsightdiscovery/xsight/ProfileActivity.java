package com.xsightdiscovery.xsight;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.network.TFSNetwork;
import com.xsightdiscovery.xsight.api.BlockUsers;
import com.xsightdiscovery.xsight.api.GetSocialMediaPrefs;
import com.xsightdiscovery.xsight.api.Models.DisplayData;
import com.xsightdiscovery.xsight.api.Models.DisplayProfile;
import com.xsightdiscovery.xsight.api.Models.User;
import com.xsightdiscovery.xsight.api.Models.DisplayString;
import com.xsightdiscovery.xsight.api.PostBlockUser;
import com.xsightdiscovery.xsight.api.PostMapPermission;
import com.xsightdiscovery.xsight.api.ProfileService;

import java.util.HashMap;
import java.util.Iterator;

import roboguice.inject.InjectView;

public class ProfileActivity extends xSightActivity implements View.OnClickListener, ProfileService.ProfileServiceDelegate {

    @Inject
    PostBlockUser postBlockUser;

    @InjectView(R.id.profilePic) private NetworkImageView profilePicture;
    @InjectView(R.id.showing_facebook) private ImageView showingFacebook;
    @InjectView(R.id.showing_linkedIn) private ImageView showingLinkedIn;
    @InjectView(R.id.showing_twitter) private ImageView showingTwitter;

    @InjectView(R.id.messageButton) private Button messageButton;
    @InjectView(R.id.findButton) private Button findButton;
    @InjectView(R.id.media_data_loading) View media_data_loading;
    @InjectView(R.id.profileLayout) LinearLayout profileLayout;

    @InjectView(R.id.profileName) TextView profileName;
    @InjectView(R.id.profileTitle1) TextView profileTitle1;
    @InjectView(R.id.profileTitle2) TextView profileTitle2;

    @Inject Global global;
    @Inject TFSNetwork network;

    @Inject ProfileService profileService;

    User user;
    Boolean isUsersProfile;     //flag for whether it is the user's profile or someone else's - true if user's, false otherwise
    final int CONCATENATION_CUTOFF = 10;

    private GoogleMap map;
    private BitmapDescriptor marker;
    private Boolean zoomedAlready = false;

    protected void onCreate(Bundle savedInstanceState) {
        flurryResID = R.string.flurry_api_key;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent i = getIntent();
        user = i.getParcelableExtra(Global.USER);

        if(user.ID.equals(global.user.ID))
            isUsersProfile = true;
        else
            isUsersProfile = false;

        messageButton.setOnClickListener(this);
        findButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((View) homeIcon.getParent()).setVisibility(View.VISIBLE);

        //remove existing profile data before loading to prevent duplication
        if(profileLayout.getChildCount() > 2)
            profileLayout.removeViews(2, profileLayout.getChildCount() - 2);

        setModeInfo();

        if(isUsersProfile)
            messageButton.setVisibility(View.GONE);

        showingFacebook.setVisibility(View.GONE);
        showingLinkedIn.setVisibility(View.GONE);
        showingTwitter.setVisibility(View.GONE);

        profilePicture.setDefaultImageResId(R.drawable.default_profile_pic);
        profilePicture.setErrorImageResId(R.drawable.default_profile_pic);
        profilePicture.setImageUrl(user.CurrentPicUrl(), network.getImageLoader());

        ab_title.setText(user.FirstName + "'s Profile");
        profileName.setText(user.FirstName + " " + user.LastName);

        profileService.fetchProfileForID(this, user);

        media_data_loading.setVisibility(View.VISIBLE);

    }

    private void setModeInfo() {
        ab_facebook.setVisibility(View.GONE);
        ab_linkedIn.setVisibility(View.GONE);
        ab_twitter.setVisibility(View.GONE);
        if (global.broadcastPrefs != null) {
            for (GetSocialMediaPrefs.SocialMediaPrefs smp : global.broadcastPrefs ) {
                if ((smp.PersonalProfile && !global.user.ProfessionalMode) || (smp.ProfessionalProfile && global.user.ProfessionalMode)) {
                    switch (smp.SocialMediumID) {
                        case 1:
                            ab_facebook.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            ab_twitter.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            ab_linkedIn.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.messageButton:
                Intent intent = new Intent(this, ChatActivity.class);
                intent.putExtra(global.USER, user);
                startActivity(intent);
                break;
            case R.id.findButton:
                if(user.IsMapVisible) {
                    global.mainActivityView = 0;
                    global.targetUser = user;
                    finish();
                }
                else {
                    new AlertDialog.Builder(this)
                            .setTitle("Sorry")
                            .setMessage("This user has chosen not to show their location on the map")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) { /* do nothing */ }
                            }).show();
                }
                break;
        }
    }

    String createColorizedText(HashMap<String, DisplayString> values, Boolean showCommonOnly) {
        String text = new String();
        int displayItemsCounter = 0;

        for(HashMap.Entry<String, DisplayString> displayStringEntry : values.entrySet()) {

            if(displayStringEntry.getValue().isInCommon) {
                if(text.length() > 0)
                    text = text + ", ";
                text = text + "<font color='blue'>" + displayStringEntry.getValue().value + "</font>";
            }
            else if(!showCommonOnly) {
                if(text.length() > 0)
                    text = text + ", ";
                text = text + displayStringEntry.getValue().value;
            }

            displayItemsCounter++;

            if(displayItemsCounter >= CONCATENATION_CUTOFF)
                break;
        }

        if(displayItemsCounter > CONCATENATION_CUTOFF)
            text = text + "...";

        return text;
    }

    @Override
    public void profileReceived(Boolean facebookFlag, Boolean twitterFlag, Boolean linkedInFlag) {
        media_data_loading.setVisibility(View.GONE);

        if(facebookFlag)
            showingFacebook.setVisibility(View.VISIBLE);
        if(twitterFlag)
            showingTwitter.setVisibility(View.VISIBLE);
        if(linkedInFlag)
            showingLinkedIn.setVisibility(View.VISIBLE);

    }

    @Override
    public void profileSorted(DisplayProfile dp) {

        try {
            if (dp.primaryTitle != null && dp.primaryTitle.length() > 0) {
                profileTitle1.setText(dp.primaryTitle);
                profileTitle1.setVisibility(View.VISIBLE);
            }

            if (dp.secondaryTitle != null && dp.secondaryTitle.length() > 0) {
                profileTitle2.setText(dp.secondaryTitle);
                profileTitle2.setVisibility(View.VISIBLE);
            }


            for (Iterator<DisplayData> iter = dp.sortedProfileData.iterator(); iter.hasNext(); ) {
                DisplayData d = iter.next();
                Boolean showCommonOnly = (!isUsersProfile && (d.typeID == 1005 || d.typeID == 16 || d.typeID == 17)) ? true : false;

                TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.profileheader, null);
                tv.setText(d.typeName);
                profileLayout.addView(tv);

                tv = (TextView) LayoutInflater.from(this).inflate(R.layout.profilebody, null);
                String text = createColorizedText(d.values, showCommonOnly);
                tv.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                profileLayout.addView(tv);
            }

            for (Iterator<DisplayData> iter = dp.profileDataTweets.iterator(); iter.hasNext(); ) {
                DisplayData d = iter.next();
                Boolean showCommonOnly = (!isUsersProfile && (d.typeID == 1005 || d.typeID == 16 || d.typeID == 17)) ? true : false;

                TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.profileheader, null);
                tv.setText(d.typeName);
                profileLayout.addView(tv);

                tv = (TextView) LayoutInflater.from(this).inflate(R.layout.profilebody, null);
                String text = createColorizedText(d.values, showCommonOnly);
                tv.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                profileLayout.addView(tv);
            }


            FrameLayout fl = (FrameLayout) LayoutInflater.from(this).inflate(R.layout.profilemap, null);
            setUpMapIfNeeded();
            profileLayout.addView(fl);

            if (!user.ID.equals(global.user.ID)) {
                TextView block_tv = (TextView) LayoutInflater.from(this).inflate(R.layout.profileheader, null);
                block_tv.setText("Block This User");
                block_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                ProfileActivity.this);

                        // set title
                        alertDialogBuilder.setTitle("xSight");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Block this user?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // if this button is clicked, close
                                        // current activity
                                        postBlockUser.Init(ProfileActivity.this.user.ID);
                                        postBlockUser.Execute(null);
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // if this button is clicked, just close
                                        // the dialog box and do nothing
                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                    }
                });
                profileLayout.addView(block_tv);

                TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.profileheader, null);
                tv.setText(Html.fromHtml(""), TextView.BufferType.SPANNABLE);
                profileLayout.addView(tv);
            }
        } catch (Exception e){
            Log.e("ProfileActivity", "Operations too fast");
            finish();
        }
    }

    public void setUpMapIfNeeded(){
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {
            // Try to obtain the map from the SupportMapFragment.
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.profileMap))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (map != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        marker = BitmapDescriptorFactory.fromResource(R.drawable.ic_active_map);
        map.addMarker(new MarkerOptions().position(new LatLng(user.Latitude, user.Longitude)).title(user.FirstName).icon(marker));

        if (!zoomedAlready) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(user.Latitude, user.Longitude), 16), 2000, null);
        } else {
            map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(user.Latitude, user.Longitude)), 2000, null);
        }
        zoomedAlready = true;
    }
}
