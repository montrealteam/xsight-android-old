package com.xsightdiscovery.xsight;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.microsoft.windowsazure.notifications.NotificationsHandler;

import com.truefitsolutions.core.network.TFSInterface;


/**
 * Created by rbrinkman on 6/17/14.
 */
public class PushNotificationHandler extends NotificationsHandler implements TFSInterface.Done {

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    Context ctx;

    @Override
    public void onRegistered(Context context, String gcmRegistrationId) {
        super.onRegistered(context, gcmRegistrationId);

        Devices registration = new Devices();
        registration.setToken(gcmRegistrationId);

        ((MainActivity)context).registerDevice.Init(registration);
        ((MainActivity)context).registerDevice.Execute(this);
    }

    @Override
    public void onReceive(Context context, Bundle bundle) {
        ctx = context;
        String nhMessage = bundle.getString("message");

        Toast.makeText(ctx, nhMessage, Toast.LENGTH_LONG).show();
        sendNotification(nhMessage);
    }

    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, new Intent(ctx, Startup.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("XSight")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    /** TFSInterface.Done **/
    @Override
    public void Done(TFSInterface i) {
        if (i.Complete()) {
            System.out.println("Registration successful");
        }
        else {
            System.out.println(i.error);
        }
    }
}
