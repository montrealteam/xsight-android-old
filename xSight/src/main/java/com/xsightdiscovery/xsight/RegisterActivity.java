package com.xsightdiscovery.xsight;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.inject.Inject;
import com.truefitsolutions.core.TFSActivity;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.xsightdiscovery.xsight.api.Register;

import roboguice.inject.InjectView;

public class RegisterActivity extends TFSActivity implements View.OnClickListener, TFSInterface.Done {

    @Inject TFSPreferenceManager preferenceManager;
    @Inject Register registerAPI;

    @InjectView(R.id.email) EditText email;
    @InjectView(R.id.password) EditText password;
    @InjectView(R.id.confirm_password) EditText confirmPassword;
    @InjectView(R.id.register) Button register;
    @InjectView(R.id.gotoLogin) Button gotoLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        flurryResID = R.string.flurry_api_key;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        register.setOnClickListener(this);
        gotoLogin.setOnClickListener(this);

        email.setText(preferenceManager.getStringPreference(TFSPreferenceManager.Preferences.email));
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case R.id.register:
                if (password.getText().toString().equals(confirmPassword.getText().toString())) {
                    preferenceManager.writeStringPreference(TFSPreferenceManager.Preferences.email, email.getText().toString());

                    registerAPI.Init("", "", email.getText().toString(), password.getText().toString(), getApplicationContext());
                    registerAPI.Execute(this);
                } else {
                    Toast.makeText(this, "Your passwords do not match.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.gotoLogin:
                finish();
                break;
        }
    }


    @Override
    public void Done(TFSInterface i) {
        if (registerAPI.Complete()) {
            startActivity(MainActivity.class);
            finish();
        } else {
            Toast.makeText(this, registerAPI.error, Toast.LENGTH_LONG).show();
        }
    }
}
