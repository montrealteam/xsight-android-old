package com.xsightdiscovery.xsight;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.inject.Inject;
import com.truefitsolutions.core.TFSActivity;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.api.ResetPassword;

import roboguice.inject.InjectView;

/**
 * Created by rbrinkman on 6/30/14.
 */
public class ResetPasswordActivity extends TFSActivity implements View.OnClickListener, TFSInterface.Done {

    @Inject ResetPassword resetPassword;

    @InjectView(R.id.emailField) EditText emailField;
    @InjectView(R.id.currentPassword) EditText currentPassword;
    @InjectView(R.id.newPassword) EditText newPassword;
    @InjectView(R.id.confirmNewPassword) EditText confirmNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        flurryResID = R.string.flurry_api_key;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        String userEmail = getIntent().getStringExtra("userEmail");
        emailField.setEnabled(false);
        emailField.setText(userEmail);

        findViewById(R.id.resetPasswordButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.resetPasswordButton) {
            if(newPassword.getText().toString().contentEquals(confirmNewPassword.getText().toString())) {
                resetPassword.Init(emailField.getText().toString(), currentPassword.getText().toString(), newPassword.getText().toString());
                resetPassword.Execute(this);
            }
            else
                Toast.makeText(this, "New password and confirm new password do not match", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void Done(TFSInterface i) {
        if (resetPassword.Complete()) {
            Toast.makeText(this, "Password changed successfully", Toast.LENGTH_LONG).show();
            finish();
        } else {
            currentPassword.setText("");
            newPassword.setText("");
            confirmNewPassword.setText("");
            Toast.makeText(this, resetPassword.error, Toast.LENGTH_LONG).show();
        }
    }
}
