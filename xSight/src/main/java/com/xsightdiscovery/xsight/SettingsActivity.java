package com.xsightdiscovery.xsight;

import org.apache.commons.lang3.StringUtils;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.google.inject.Inject;
import com.truefitsolutions.core.helpers.TFSDialog;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.network.TFSNetwork;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.truefitsolutions.core.social.DialogListener;
import com.truefitsolutions.core.social.SocialAuthAdapter;
import com.truefitsolutions.core.social.SocialAuthError;
import com.xsightdiscovery.xsight.api.GetSocialMediaDataPrefs;
import com.xsightdiscovery.xsight.api.PostMapPermission;
import com.xsightdiscovery.xsight.api.SetMyInfo;
import com.xsightdiscovery.xsight.api.SetSocialMediaDataPrefs;
import com.xsightdiscovery.xsight.api.SetSocialMediaPrefs;
import com.xsightdiscovery.xsight.api.SetSocialMediaProfile;
import com.xsightdiscovery.xsight.api.XSightSocialMediaAdapter;

import java.util.List;

import roboguice.inject.InjectView;

public class SettingsActivity extends xSightActivity implements View.OnClickListener, TFSInterface.Done {

    @Inject TFSPreferenceManager preferenceManager;
    @Inject public TFSNetwork network;

    @Inject SetSocialMediaPrefs setSocialMediaPrefs;
    @Inject GetSocialMediaDataPrefs getSocialMediaDataPrefs;
    @Inject SetSocialMediaDataPrefs setSocialMediaDataPrefs;
    @Inject SetSocialMediaProfile setSocialMediaProfile;

    @Inject PostMapPermission postMapPermission;
    @Inject SetMyInfo setMyInfo;
    @Inject Global global;

    @InjectView(R.id.locationMapSwitch) Switch locationToggleSwitch;

    @InjectView(R.id.facebook) ImageButton facebook;
    @InjectView(R.id.twitter) ImageButton twitter;
    @InjectView(R.id.linkedin) ImageButton linkedin;

    @InjectView(R.id.facebook_checked)ImageView facebook_checked;
    @InjectView(R.id.twitter_checked) ImageView twitter_checked;
    @InjectView(R.id.linkedin_checked) ImageView linkedin_checked;

    @InjectView(R.id.media_data_instructions) View media_data_instructions;
    @InjectView(R.id.media_data_instructions_no_connections) View media_data_instructions_no_connections;
    @InjectView(R.id.media_data_loading) View media_data_loading;

    @InjectView(R.id.media_data_list) ListView list;

    Button settings_save;
    Button settings_remove;

    MediaDataAdapter adapter;
    View footer;

    Boolean facebookConnected;
    Boolean twitterConnected;
    Boolean linkedInConnected;

    private static XSightSocialMediaAdapter socialAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Footer has to be added before adapter....
        footer = View.inflate(this, R.layout.view_media_footer, null);
        footer.setVisibility(View.GONE);
        settings_save = (Button)footer.findViewById(R.id.settings_save);
        settings_remove = (Button)footer.findViewById(R.id.settings_remove);
        list.addFooterView(footer);

        adapter = new MediaDataAdapter(this, null, -1);
        list.setAdapter(adapter);
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        linkedin.setOnClickListener(this);
        settings_save.setOnClickListener(this);
        settings_remove.setOnClickListener(this);

        facebookConnected = preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.FACEBOOK.toString(), false);
        twitterConnected = preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.TWITTER.toString(), false);
        linkedInConnected = preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.LINKEDIN.toString(), false);

        if (!facebookConnected && !twitterConnected && !linkedInConnected) {
            media_data_instructions.setVisibility(View.GONE);
            media_data_instructions_no_connections.setVisibility(View.VISIBLE);
        } else {
            media_data_instructions.setVisibility(View.VISIBLE);
            media_data_instructions_no_connections.setVisibility(View.GONE);
        }

        socialAdapter = new XSightSocialMediaAdapter(new SocialResponseListener(), global);

        locationToggleSwitch.setChecked(global.user.IsMapVisible);
        locationToggleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                global.user.IsMapVisible = isChecked;
                postUpdatedMapPermission();    //notify the server
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        ab_facebook.setVisibility(View.GONE);
        ab_linkedIn.setVisibility(View.GONE);
        ab_twitter.setVisibility(View.GONE);
        ab_title.setVisibility(View.VISIBLE);
        ab_title.setText("Settings");
        ((View) homeIcon.getParent()).setVisibility(View.VISIBLE);

        displayItems();

        getSocialMediaDataPrefs.Init();
        getSocialMediaDataPrefs.Execute(this);
    }

    private void displayItems() {
        facebookConnected = preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.FACEBOOK.toString(), false);
        twitterConnected = preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.TWITTER.toString(), false);
        linkedInConnected = preferenceManager.getBooleanPreference(SocialAuthAdapter.Provider.LINKEDIN.toString(), false);

        facebook_checked.setVisibility(facebookConnected ? View.VISIBLE : View.GONE);
        twitter_checked.setVisibility(twitterConnected ? View.VISIBLE : View.GONE);
        linkedin_checked.setVisibility(linkedInConnected ? View.VISIBLE : View.GONE);
    }

    void postUpdatedMapPermission() {
        postMapPermission.Init();
        postMapPermission.Execute(this);
    }

    public void activate(View v){
        ViewGroup vg = (ViewGroup)v.getParent();

        for (int i = 0; i < vg.getChildCount(); i++) {
            View ov = vg.getChildAt(i);
            if (ov.getClass() == ImageButton.class) {
                ov.setActivated(false);
            }
        }
        v.setActivated(true);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.settings_save:

                String mode1 = StringUtils.capitalize(getResources().getString(R.string.title_section2).toLowerCase()) + " Mode";
                String mode2 = StringUtils.capitalize(getResources().getString(R.string.title_section1).toLowerCase()) + " Mode";

                TFSDialog d = new TFSDialog(this, "Settings", "Set this social media outlet to display in:", "Both", mode1, mode2,
                        new TFSDialog.TFSDialogListener() {
                            public void onDialogClick(TFSDialog dialog, int whichButton) {
                                setSocialMediaPrefs.Init(adapter.getSocialMediumID(),
                                        (whichButton == DialogInterface.BUTTON_POSITIVE || whichButton == DialogInterface.BUTTON_NEGATIVE),
                                        (whichButton == DialogInterface.BUTTON_POSITIVE || whichButton == DialogInterface.BUTTON_NEUTRAL) );
                                setSocialMediaPrefs.Execute(SettingsActivity.this);

                                global.setSocialMediaPrefs(adapter.getSocialMediumID(),
                                        (whichButton == DialogInterface.BUTTON_POSITIVE || whichButton == DialogInterface.BUTTON_NEGATIVE),
                                        (whichButton == DialogInterface.BUTTON_POSITIVE || whichButton == DialogInterface.BUTTON_NEUTRAL) );

                                socialAdapter.authorize(SettingsActivity.this, adapter.getSocialMediumProvider());
                                setSocialMediaDataPrefs.Init(getSocialMediaDataPrefs.result);
                                setSocialMediaDataPrefs.Execute(SettingsActivity.this);
                                media_data_loading.setVisibility(View.VISIBLE);
                            }
                        });
                d.Show();
                break;
            case R.id.settings_remove:
                socialAdapter.signOut(this, adapter.getSocialMediumProvider().toString());

                setSocialMediaPrefs.Init(adapter.getSocialMediumID(), false, false );
                setSocialMediaPrefs.Execute(SettingsActivity.this);

                global.setSocialMediaPrefs(adapter.getSocialMediumID(), false, false);

                preferenceManager.writeBooleanPreference(adapter.getSocialMediumProvider().toString(), false );
                settings_remove.setVisibility(View.GONE);

                if (adapter.getSocialMediumProvider().toString().equalsIgnoreCase("facebook")) {
                    global.user.FacebookPicUrl = null;
                }
                if (adapter.getSocialMediumProvider().toString().equalsIgnoreCase("twitter")) {
                    global.user.TwitterPicUrl = null;
                }
                if (adapter.getSocialMediumProvider().toString().equalsIgnoreCase("linkedin")) {
                    global.user.LinkedInPicUrl = null;
                }
                displayItems();

                Toast.makeText(SettingsActivity.this, adapter.getSocialMediumProvider().name() + " removed", Toast.LENGTH_SHORT).show();
                break;
            default:
                activate(v);

                switch(v.getId()) {
                    case R.id.facebook:
                        adapter.setSocialMediumID(1);
                        settings_remove.setVisibility(facebookConnected ? View.VISIBLE : View.GONE);
                        break;
                    case R.id.twitter:
                        adapter.setSocialMediumID(2);
                        settings_remove.setVisibility(twitterConnected ? View.VISIBLE : View.GONE);
                        break;
                    case R.id.linkedin:
                        adapter.setSocialMediumID(3);
                        settings_remove.setVisibility(linkedInConnected ? View.VISIBLE : View.GONE);
                        break;
                }

                footer.setVisibility(View.VISIBLE);
                media_data_instructions.setVisibility(View.GONE);
                media_data_instructions_no_connections.setVisibility(View.GONE);
                if (getSocialMediaDataPrefs.Executing()) {
                    media_data_loading.setVisibility(View.VISIBLE);
                }
        }
    }

    @Override
    public void Done(TFSInterface i) {
        media_data_loading.setVisibility(View.GONE);

        if (i instanceof GetSocialMediaDataPrefs) {
            if (i.Complete()) {
                // Fill in the list view
                adapter.setItems(getSocialMediaDataPrefs.result);
            } else {
                Toast.makeText(this, "Unable to load social media settings.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }

        if (i instanceof SetSocialMediaDataPrefs) {
            if (i.Complete()) {
                Toast.makeText(this, "Settings Saved.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Unable to save social media settings.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }

        if (i instanceof SetMyInfo) {
            if (i.Complete()) {
                Toast.makeText(this, "Settings Saved.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Unable to save user settings.  Please try again later. (" + i.error + ")", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // To receive the response after authentication
    private final class SocialResponseListener implements DialogListener {

        @Override
        public void onComplete(Bundle values) {

            // Show Remove button
            preferenceManager.writeBooleanPreference(adapter.getSocialMediumProvider().toString(), true );
            settings_remove.setVisibility(View.VISIBLE);

            displayItems();

            // Let them know we connected
            SocialAuthAdapter.Provider provider = adapter.getSocialMediumProvider();
            Toast.makeText(SettingsActivity.this, provider.name() + " connected", Toast.LENGTH_SHORT).show();

            // Pull Info
            socialAdapter.GetAndSaveSocialMediaProfile();
        }

        @Override
        public void onError(SocialAuthError error) {

            // Hide Remove button
            preferenceManager.writeBooleanPreference(adapter.getSocialMediumProvider().toString(), false );
            settings_remove.setVisibility(View.GONE);

            displayItems();

            // Let them know we failed to connect
            SocialAuthAdapter.Provider provider = adapter.getSocialMediumProvider();
            final String message = provider.name() + " not connected: " + error.getInnerException().getLocalizedMessage();
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(SettingsActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onCancel() {
            Log.d("Custom-UI", "Cancelled");
        }

        @Override
        public void onBack() {
            Log.d("Custom-UI", "Dialog Closed by pressing Back Key");
        }

        @Override
        public void onAllDone(List<SetSocialMediaProfile.Request> profile) {    //called from dialog listener
            if (profile != null) {
                setSocialMediaProfile.Init(profile.toArray(new SetSocialMediaProfile.Request[profile.size()]));
                setSocialMediaProfile.Execute(SettingsActivity.this);

                setMyInfo.Init();
                setMyInfo.Execute(SettingsActivity.this);
            }
        }
    }
}