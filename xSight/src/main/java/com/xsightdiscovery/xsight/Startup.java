package com.xsightdiscovery.xsight;

import android.app.Activity;
import android.os.Bundle;

import com.google.inject.Inject;
import com.truefitsolutions.core.TFSStartup;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.xsightdiscovery.xsight.api.Login;

import java.util.Arrays;

public class Startup extends TFSStartup implements TFSInterface.Done {

    private boolean loginSuccess = false;

    @Inject TFSPreferenceManager pm;
    @Inject Login login;

    protected void onCreate(Bundle savedInstanceState){
        flurryResID = R.string.flurry_api_key;
        images = Arrays.asList(R.drawable.splash_1);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void startupApp()
    {
        // Do Login
        login.Init(pm.getStringPreference(TFSPreferenceManager.Preferences.email), pm.getStringPreference(TFSPreferenceManager.Preferences.password), getApplicationContext());
        login.Execute(this);
    }

    @Override
    public void Done(TFSInterface i) {
        loginSuccess = login.Complete();
        super.startupApp();
    }

    @Override
    protected void continueApp(Class<? extends Activity> startClass)
    {
        if (loginSuccess) {
            startClass = MainActivity.class;
        } else {
            startClass = LoginActivity.class;
        }
        super.continueApp(startClass);
    }

}