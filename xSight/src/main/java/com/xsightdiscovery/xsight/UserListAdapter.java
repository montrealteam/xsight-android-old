package com.xsightdiscovery.xsight;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSNetwork;
import com.xsightdiscovery.xsight.api.Models.User;

import roboguice.RoboGuice;

public class UserListAdapter extends BaseAdapter {

    private Context c;
    private User[] items;
    @Inject TFSNetwork network;

    public UserListAdapter(Context c, User[] items){
        this.c = c;
        this.items = items;
        RoboGuice.getInjector(c).injectMembers(this);
    }

    public void setItems(User[] items){
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (items == null) ? 0 : items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        User x = items[position];
        return x.ID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(c, R.layout.view_user_list, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.user_list_name);
            holder.unreadChatsCount = (TextView)convertView.findViewById(R.id.unread_counter);
            holder.distance = (TextView) convertView.findViewById(R.id.user_list_distance);
            holder.image = (NetworkImageView)convertView.findViewById(R.id.user_list_image);
            holder.image.setDefaultImageResId(R.drawable.default_profile_pic);
            holder.image.setErrorImageResId(R.drawable.default_profile_pic);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        User user = items[position];

        holder.name.setText(user.FirstName + " " + user.LastName );
        holder.unreadChatsCount.setText(user.UnreadChats.toString());
        if(user.Distance != null){
            if(user.Distance <= 500)
                holder.distance.setText("less than" + "0.5 km");
            else
                holder.distance.setText(Math.round(user.Distance*10/1000)/10.0+" km");

        }

        holder.image.setImageUrl(user.CurrentPicUrl(), network.getImageLoader());

        if(user.UnreadChats == 0)
            holder.unreadChatsCount.setVisibility(View.INVISIBLE);
        else
            holder.unreadChatsCount.setVisibility(View.VISIBLE);

        return convertView;
    }

    /** Helper classes **/
    /*private view holder class*/
    private class ViewHolder {
        TextView name;
        TextView unreadChatsCount;
        TextView distance;
        NetworkImageView image;
    }
}
