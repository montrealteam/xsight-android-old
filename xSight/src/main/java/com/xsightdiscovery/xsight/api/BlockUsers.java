package com.xsightdiscovery.xsight.api;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by yucunli on 2015-03-04.
 */
public class BlockUsers {

    Context mContext;

    public BlockUsers(Context context){
        this.mContext = context;
    }

    public List<Integer> getBlockUser(){
        List<Integer> result = new LinkedList<Integer>();

        SharedPreferences mySharedPreferences= mContext.getSharedPreferences("xSight_block_users",Activity.MODE_PRIVATE);
        Map<String, Integer> allEntries = (Map<String, Integer>) mySharedPreferences.getAll();
        for(Map.Entry<String, Integer> entry : allEntries.entrySet()){
            result.add(entry.getValue());
        }
        return result;
    }
}
