package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

/**
 * Created by rbrinkman on 6/29/14.
 */
public class ForgotPassword extends TFSInterface {

    @Inject Global global;

    private static String baseUrl = "https://xsight.azure-mobile.net/tables/users/?forgotPassword=true";
    public Request request;
    public Object result;


    @Inject public ForgotPassword() {
        super();
        super.InitPost(baseUrl, Object.class);
    }

    public void Init(String email) {
        request = new Request();
        request.email = email;
        oRequest = request;
    }

    @Override
    public void Execute(TFSInterface.Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-application", "jRUtuYFLqnLtCxQcpBCevGpDRnLBkF32");

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = oResult;
        super.Done(i);
    }

    public class Request {
        public String email;
    }
}
