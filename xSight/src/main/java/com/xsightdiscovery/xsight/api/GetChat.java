package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.Chat;

import java.util.HashMap;

public class GetChat extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/chats?withUser=";
    public Chat[] chats;
    private Integer user;

    @Inject public GetChat() {
        super();
        super.InitGet(myUrl, Chat[].class);
    }

    public void Init(Integer user) {
        oRequest = null;
        this.user = user;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done, user.toString());
    }

    @Override
    protected void Done(TFSInterface i) {
        chats = (Chat[])oResult;
        for (Chat c : chats) {
            c.Clean();
        }
        super.Done(i);
    }
}

