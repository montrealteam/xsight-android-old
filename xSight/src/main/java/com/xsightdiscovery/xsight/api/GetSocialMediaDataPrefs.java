package com.xsightdiscovery.xsight.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class GetSocialMediaDataPrefs extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/api/socialmediadatatypepreferences";
    private final Gson gson = new Gson();
    public SocialMediaDataPrefs[] result;

    @Inject public GetSocialMediaDataPrefs() {
        super();
        super.InitGet(myUrl, SocialMediaDataPrefs[].class);
    }

    public void Init() {
        oRequest = null;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (SocialMediaDataPrefs[])oResult;
        super.Done(i);
    }

    public class SocialMediaType {
        public Integer ID;
        public Integer SocialMediumID;
        public String Name;
    }

    public class SocialMediaDataPrefs {
        public Integer ID;
        public Integer UserID;
        public Integer Share;
        public SocialMediaType Type;
    }

}

