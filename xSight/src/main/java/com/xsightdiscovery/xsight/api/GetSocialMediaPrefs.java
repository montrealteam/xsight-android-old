package com.xsightdiscovery.xsight.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class GetSocialMediaPrefs extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/socialmediapreferences";
    private final Gson gson = new Gson();
    public SocialMediaPrefs[] result;

    @Inject public GetSocialMediaPrefs() {
        super();
        super.InitGet(myUrl, SocialMediaPrefs[].class);
    }

    public void Init() {
        oRequest = null;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (SocialMediaPrefs[])oResult;
        if (this.Complete()) {
            global.broadcastPrefs = this.result;
        }
        super.Done(i);
    }

    public class SocialMediaPrefs {
        public Integer ID;
        public Integer UserID;
        public Integer SocialMediumID;
        public Boolean ProfessionalProfile;
        public Boolean PersonalProfile;
    }

}

