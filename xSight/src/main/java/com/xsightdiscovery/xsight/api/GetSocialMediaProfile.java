package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.SocialMediaData;

import java.util.HashMap;

public class GetSocialMediaProfile extends TFSInterface {

    @Inject Global global;

    private static String requestURL;
    public SocialMediaData[] result;
    private Integer user;

    @Inject public GetSocialMediaProfile() {
        super();
    }

    public void Init(Integer user) {
        if(user == global.user.ID)
            requestURL = "https://xsight.azure-mobile.net/api/fullprofile?userID=" + user.toString();
        else
            requestURL = "https://xsight.azure-mobile.net/api/fullprofile?userID=" + user.toString() + "&filterProfile=true";

        super.InitGet(requestURL, SocialMediaData[].class);

        oRequest = null;
        this.user = user;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done, "");
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (SocialMediaData[])oResult;
        super.Done(i);
    }
}
