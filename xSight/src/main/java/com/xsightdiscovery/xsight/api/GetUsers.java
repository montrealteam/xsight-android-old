package com.xsightdiscovery.xsight.api;

import android.content.Context;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GetUsers extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/users?filterByLocation=true&dist=";
    public User[] users;
    private Float range;
    private Context mContext;

    @Inject public GetUsers() {
        super();
        super.InitGet(myUrl, User[].class);
    }

    public void Init(float range) {
        oRequest = null;
        this.range = range;
    }

    // TODO: Miles to Kilometers?
    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done, range.toString() + "&lat=" + global.user.Latitude + "&lon=" + global.user.Longitude);
    }

    @Override
    protected void Done(TFSInterface i) {
        if(oResult != null) {
            users = (User[]) oResult;
            for (User u : users) {
                u.Clean();
            }

            List<User> filteredUser = new LinkedList<User>();
            for(User user : users){
                if(!user.IsBlocked){
                    filteredUser.add(user);
                }
            }

            users = new User[filteredUser.size()];
            for(int index =0; index<filteredUser.size(); index++){
                users[index] = filteredUser.get(index);
            }

            Arrays.sort(users, new Comparator<User>() {
                        @Override
                        public int compare(User lhs, User rhs) {

                            int lhs_distance = computeGeoDistance(lhs, global.user);
                            lhs.Distance = lhs_distance;
                            int rhs_distance = computeGeoDistance(rhs, global.user);
                            rhs.Distance = rhs_distance;

                            return lhs_distance - rhs_distance;
                        }
                    }
            );
        }
        super.Done(i);
    }

    private int computeGeoDistance(User user1, User user2){
        double earthRadius = 6371000;
        double dLat = Math.toRadians(user2.Latitude - user1.Latitude);
        double dLng = Math.toRadians(user2.Longitude - user1.Longitude);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.toRadians(user1.Latitude)) * Math.cos(Math.toRadians(user2.Latitude)) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);
        return Math.round(dist);
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }
}

