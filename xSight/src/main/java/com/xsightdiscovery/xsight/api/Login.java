package com.xsightdiscovery.xsight.api;

import android.content.Context;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.User;

import java.util.HashMap;

public class Login extends TFSInterface {

    @Inject GetSocialMediaPrefs getSocialMediaPrefs;
    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/users?login=true";
    private Request request;
    private User user;

    TFSPreferenceManager preferenceManager;

    @Inject public Login() {
        super();
        super.Init(myUrl, User.class);
    }

    public void Init(String email, String password, Context context) {
        request = new Request();
        request.email = email;
        request.password = password;
        oRequest = request;

        preferenceManager = new TFSPreferenceManager(context);
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-application", "jRUtuYFLqnLtCxQcpBCevGpDRnLBkF32");

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        user = (User)oResult;
        if (this.Complete()) {
            this.user.Clean();
            global.user = this.user;
            preferenceManager.writeStringPreference(TFSPreferenceManager.Preferences.password, request.password);
            preferenceManager.writeIntPreference(TFSPreferenceManager.Preferences.userID, global.user.ID);

            getSocialMediaPrefs.Init();
            getSocialMediaPrefs.Execute(null);
        }
        super.Done(i);
    }

    public class Request {
        String email;
        String password;
    }
}
