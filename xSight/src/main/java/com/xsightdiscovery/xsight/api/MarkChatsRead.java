package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class MarkChatsRead extends TFSInterface {

    @Inject Global global;

    private static String baseURL = "https://xsight.azure-mobile.net/";
    private static String markMessagesReadURL = "api/readchats?userID=";
    public UnreadChat unreadChat;
    private Integer user;

    @Inject public MarkChatsRead() {
        super();
        super.InitPost(baseURL + markMessagesReadURL, UnreadChat.class);
    }

    public void Init(Integer user) {
        oRequest = null;
        this.user = user;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done, user.toString());
    }

    @Override
    protected void Done(TFSInterface i) {
        unreadChat = (UnreadChat)oResult;
        super.Done(i);
    }

    public class UnreadChat {
        public String result;
        public String unreadChats;
    }
}
