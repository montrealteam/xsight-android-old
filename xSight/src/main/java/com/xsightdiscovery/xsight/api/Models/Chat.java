package com.xsightdiscovery.xsight.api.Models;

import java.text.SimpleDateFormat;

/**
 * Created by rbrinkman on 4/24/14.
 */

public class Chat implements Comparable<Chat> {
    public Integer ID;
    public Integer FromUserID;
    public Integer ToUserID;
    public String Message;
    public String Date;
    public java.util.Date calcDate;

    @Override
    public int compareTo(Chat c) {
        if(calcDate != null && c.calcDate != null) {
            return calcDate.compareTo(c.calcDate);
        }
        else return 0;
    }

    public void Clean() {
        //"2014-01-06T15:13:50.965Z"
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            calcDate = format.parse(Date);
        } catch (Exception e) {
            calcDate = null;
        }
    }
}