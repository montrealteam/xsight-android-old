package com.xsightdiscovery.xsight.api.Models;

import java.util.HashMap;

/**
 * Created by rbrinkman on 6/12/14.
 */
public class DisplayData {

    public HashMap<String, DisplayString> values;
    public Integer typeID;
    public String typeName;

    public int numberOfConcatenatedItems;

    public DisplayData (DisplayString value, Integer typeID, String typeName) {
        this.values = new HashMap<String, DisplayString>();
        this.values.put(value.value, value);
        this.typeID = typeID;
        this.typeName = typeName;

        numberOfConcatenatedItems = 1;
    }
}
