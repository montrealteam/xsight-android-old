package com.xsightdiscovery.xsight.api.Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by rbrinkman on 6/12/14.
 */
public class DisplayProfile {

    public User user;

    public LinkedList<DisplayData> profileDataTweets = new LinkedList<DisplayData>();
    public HashMap<Integer, DisplayData> profileDataGrouped = new HashMap<Integer, DisplayData>();
    public HashMap<String, DisplayData> profileDataMerged = new HashMap<String, DisplayData>();
    public ArrayList<DisplayData> sortedProfileData;

    public String primaryTitle;
    public String secondaryTitle;

    public DisplayProfile(User u) {
        user = u;
    }
}
