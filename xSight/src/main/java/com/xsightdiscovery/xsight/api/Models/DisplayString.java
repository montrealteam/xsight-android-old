package com.xsightdiscovery.xsight.api.Models;

/**
 * Created by rbrinkman on 6/12/14.
 */
public class DisplayString {

    public String value;
    public Boolean isInCommon;

    public DisplayString(String value, Boolean isInCommon) {
        this.value = value;
        this.isInCommon = isInCommon;
    }
}