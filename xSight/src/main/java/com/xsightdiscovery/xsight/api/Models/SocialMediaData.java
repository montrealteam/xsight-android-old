package com.xsightdiscovery.xsight.api.Models;

import java.util.ArrayList;

/**
 * Created by rbrinkman on 10/20/14.
 */
public class SocialMediaData {
    public Integer ID;
    public Integer ParentID;

    public String Value;
    public String StartDate;
    public String EndDate;
    public String CustomID;

    public SocialMediaType Type;
    public ArrayList<String> Children;

    public Boolean matched;
}
