package com.xsightdiscovery.xsight.api.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    public Integer ID;
    public String Email;
    public String FirstName;
    public String LastName;
    public Boolean Active;
    public Boolean ProfessionalMode;
    public String TwitterPicUrl;
    public String FacebookPicUrl;
    public String LinkedInPicUrl;
    public Boolean IsMapVisible;
    public Boolean IsBlocked;
    public Double Latitude;
    public Double Longitude;
    public Integer UnreadChats;
    public String Token;

    public Integer Distance;

    public User(Integer ID, String Email, String FirstName, String LastName, Boolean Active,
                Boolean ProfessionalMode, Boolean IsMapVisible, Boolean IsBlocked, Double Latitude, Double Longitude, Integer UnreadChats, String Token) {
        this.ID = ID;
        this.Email = Email;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Active = Active;
        this.ProfessionalMode = ProfessionalMode;
        this.IsMapVisible = IsMapVisible;
        this.IsBlocked = IsBlocked;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.UnreadChats = UnreadChats;
        this.Token = Token;
    }

    public String CurrentPicUrl() {
        if (ProfessionalMode) {
            if (LinkedInPicUrl != null) {
                return LinkedInPicUrl;
            } else if (FacebookPicUrl != null) {
                return FacebookPicUrl;
            } else if (TwitterPicUrl != null) {
                return TwitterPicUrl;
            } else {
                return "";
            }
        } else {
            if (FacebookPicUrl != null) {
                return FacebookPicUrl;
            } else if (TwitterPicUrl != null) {
                return TwitterPicUrl;
            } else if (LinkedInPicUrl != null) {
                return LinkedInPicUrl;
            } else {
                return "";
            }
        }
    }

    public void Clean() {
        if (TwitterPicUrl != null && TwitterPicUrl.equals("<null>"))
            TwitterPicUrl = null;
        if (FacebookPicUrl != null && FacebookPicUrl.equals("<null>"))
            FacebookPicUrl = null;
        if (LinkedInPicUrl != null && LinkedInPicUrl.equals("<null>"))
            LinkedInPicUrl = null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(Email);
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeByte((byte) (Active ? 1 : 0));
        dest.writeByte((byte) (ProfessionalMode ? 1 : 0));
        dest.writeString(TwitterPicUrl);
        dest.writeString(FacebookPicUrl);
        dest.writeString(LinkedInPicUrl);
        dest.writeByte((byte) (IsMapVisible ? 1 : 0));
        dest.writeByte((byte) (IsBlocked ? 1 : 0));
        dest.writeDouble(Latitude);
        dest.writeDouble(Longitude);
        dest.writeInt(UnreadChats != null ? UnreadChats : 0);
        dest.writeString(Token);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private User(Parcel in) {
        ID = in.readInt();
        Email = in.readString();
        FirstName = in.readString();
        LastName = in.readString();
        Active = in.readByte() != 0;
        ProfessionalMode = in.readByte() != 0;
        TwitterPicUrl = in.readString();
        FacebookPicUrl = in.readString();
        LinkedInPicUrl = in.readString();
        IsMapVisible = in.readByte() != 0;
        IsBlocked = in.readByte() != 0;
        Latitude = in.readDouble();
        Longitude = in.readDouble();
        UnreadChats = in.readInt();
        Token = in.readString();
    }
}
