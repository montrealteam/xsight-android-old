package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

/**
 * Created by anasambri on 15-08-04.
 */
public class PostBlockUser extends TFSInterface {

    @Inject
    Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/blockeduserdetail";
    public Request request;
    public Result result;

    @Inject public PostBlockUser() {
        super();
        super.InitPost(myUrl, Result.class);
    }

    public void Init(Integer ID) {
        request = new Request();
        request.isToBeBlocked = "true";
        request.blockedUserId = String.valueOf(ID);
        oRequest = request;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);
        headers.put("X-HTTP-Method-Override", "POST");

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (Result)oResult;
        super.Done(i);
    }

    private class Request {
        public String isToBeBlocked;
        public String blockedUserId;
    }

    private class Result {
    }

}
