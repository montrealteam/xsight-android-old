package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

/**
 * Created by rbrinkman on 10/16/14.
 */
public class PostMapPermission extends TFSInterface {

    @Inject
    Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/users/";
    public Request request;
    public Result result;

    @Inject public PostMapPermission() {
        super();
        super.InitPost(myUrl, Result.class);
    }

    public void Init() {
        request = new Request();
        request.isMapVisible = global.user.IsMapVisible;
        oRequest = request;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);
        headers.put("X-HTTP-Method-Override", "PATCH");

        super.Execute(done, global.user.ID.toString());
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (Result)oResult;
        if (result != null) {
            global.user.IsMapVisible = result.IsMapVisible;
        }
        super.Done(i);
    }

    private class Request {
        public Boolean isMapVisible;
    }

    private class Result {
        public Integer ID;
        public Boolean IsMapVisible;
    }

}

