package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.DisplayData;
import com.xsightdiscovery.xsight.api.Models.DisplayProfile;
import com.xsightdiscovery.xsight.api.Models.DisplayString;
import com.xsightdiscovery.xsight.api.Models.SocialMediaData;
import com.xsightdiscovery.xsight.api.Models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by rbrinkman on 6/12/14.
 */
public class ProfileService implements TFSInterface.Done {

    public abstract static interface ProfileServiceDelegate {
        public abstract void profileReceived(Boolean facebookFlag, Boolean twitterFlag, Boolean linkedInFlag);
        public abstract void profileSorted(DisplayProfile dp);
    }

    @Inject GetSocialMediaProfile getSocialMediaProfile;
    @Inject Global global;

    DisplayProfile displayProfile;
    ProfileServiceDelegate delegate;

    Boolean isUsersProfile;     //flag for whether it is the user's profile or someone else's - true if user's, false otherwise

    private int friendsCount;
    private int followersCount;
    private int connectionsCount;

    public void fetchProfileForID(ProfileServiceDelegate d, User userToFetch) {

        friendsCount = 0;
        followersCount = 0;
        connectionsCount = 0;

        displayProfile = new DisplayProfile(userToFetch);
        delegate = d;

        if(userToFetch.ID.equals(global.user.ID))
            isUsersProfile = true;
        else
            isUsersProfile = false;

        getSocialMediaProfile.Init(userToFetch.ID);
        getSocialMediaProfile.Execute(this);
    }

    Boolean checkIfInCommon(SocialMediaData socialMediaData) {
        if(isUsersProfile || global.displayProfile == null)
            return false;   //don't want to show blue matching text for user's own profile

        for(int i = 0; i < global.displayProfile.sortedProfileData.size(); i++) {
            if(global.displayProfile.sortedProfileData.get(i).values.containsKey(socialMediaData.Value))
                return true;
        }
        return false;
    }

    void placeGroupedItemInArray(DisplayData item, HashMap<Integer, DisplayData> array) {

        if(array.containsKey(item.typeID)) {
            DisplayData d = array.get(item.typeID);

            d.values.putAll(item.values);
            d.numberOfConcatenatedItems++;

            if(d.typeID == 1005 || d.typeID == 16 || d.typeID == 17) {

                //update title count if needed
                updateTitleCounts(d);

                //only show friends/followers/connections if it is the user's own profile, or the item is in-common
                DisplayString ds = (DisplayString) d.values.values().toArray()[0];
                if(!isUsersProfile && !ds.isInCommon)
                    d.values.remove(ds.value);
            }

            array.put(d.typeID, d);
        }
        else {
            //only show friends/followers/connections if it is the user's own profile
            DisplayString ds = (DisplayString) item.values.values().toArray()[0];

            //update title count if needed
            updateTitleCounts(item);

            if(!isUsersProfile && (item.typeID == 1005 || item.typeID == 16 || item.typeID == 17) && !ds.isInCommon)
                item.values.remove(ds.value);

            array.put(item.typeID, item);
        }
    }

    private void updateTitleCounts(DisplayData item) {
        String title;
        switch (item.typeID) {
            case 1005:
                title = "Friends - ";
                friendsCount++;
                item.typeName = title + friendsCount;
                break;
            case 16:
                title = "Followers - ";
                followersCount++;
                item.typeName = title + followersCount;
                break;
            case 17:
                title = "Connections - ";
                connectionsCount++;
                item.typeName = title + connectionsCount;
                break;
        }
    }

    @Override
    public void Done(TFSInterface i) {

        if (i instanceof GetSocialMediaProfile) {
            if (i.Complete()) {
                // Fill in the list view

                Boolean showingFacebook = false;
                Boolean showingTwitter = false;
                Boolean showingLinkedIn = false;

                for (SocialMediaData smd : getSocialMediaProfile.result) {

                    switch (smd.Type.SocialMediumID) {
                        case 1:
                            showingFacebook = true;
                            break;
                        case 2:
                            showingTwitter = true;
                            break;
                        case 3:
                            showingLinkedIn = true;
                            break;
                    }

                    if (smd.Value != null && smd.Value.length() > 0 && smd.Type.ID != 23 && smd.Type.ID != 14 && smd.Type.ID != 29) {
                        String currentValue = smd.Value.replace("&amp;", "&");
                        DisplayData displayData = new DisplayData(new DisplayString(currentValue, checkIfInCommon(smd)), smd.Type.ID, smd.Type.Name);

                        if (smd.Type.SocialMediumID == 3 && smd.Type.ID == 21)
                            displayProfile.primaryTitle = currentValue;
                        else if (smd.Type.SocialMediumID == 3 && smd.Type.ID == 22)
                            displayProfile.secondaryTitle = currentValue;
                        else if (smd.Type.ID == 15)
                            displayProfile.profileDataTweets.add(displayData);
                        else
                            placeGroupedItemInArray(displayData, displayProfile.profileDataGrouped);
                    }
                }

                delegate.profileReceived(showingFacebook, showingTwitter, showingLinkedIn);

                //merge any duplicated data from across social media profiles
                for(HashMap.Entry<Integer, DisplayData> displayDataEntry : displayProfile.profileDataGrouped.entrySet()) {
                    if(displayProfile.profileDataMerged.containsKey(displayDataEntry.getValue().typeName)) {
                        DisplayData d = displayProfile.profileDataMerged.get(displayDataEntry.getValue().typeName);

                        DisplayData mergedData = displayDataEntry.getValue();
                        mergedData.values.putAll(d.values);

                        displayProfile.profileDataMerged.put(mergedData.typeName, mergedData);
                    }
                    else {
                        displayProfile.profileDataMerged.put(displayDataEntry.getValue().typeName, displayDataEntry.getValue());
                    }
                }

                //alphabetize merged profile data
                displayProfile.sortedProfileData = new ArrayList<DisplayData>(displayProfile.profileDataMerged.values());
                Collections.sort(displayProfile.sortedProfileData, new Comparator<DisplayData>() {
                    @Override
                    public int compare(DisplayData displayData, DisplayData displayData2) {
                        return displayData.typeName.compareToIgnoreCase(displayData2.typeName);
                    }
                });

                delegate.profileSorted(displayProfile);
            }
        }
    }

}
