package com.xsightdiscovery.xsight.api;

import android.content.Context;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.truefitsolutions.core.preferences.TFSPreferenceManager;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.User;

import java.util.HashMap;

public class Register extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/users";
    private Request request;
    private RegisterUser user;

    TFSPreferenceManager preferenceManager;

    @Inject public Register() {
        super();
        super.Init(myUrl, RegisterUser.class);
    }

    public void Init(String firstName, String lastName, String email, String password, Context context) {
        request = new Request();
        request.firstName = firstName;
        request.lastName = lastName;
        request.email = email;
        request.password = password;
        oRequest = request;

        preferenceManager = new TFSPreferenceManager(context);
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-application", "jRUtuYFLqnLtCxQcpBCevGpDRnLBkF32");

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        user = (RegisterUser)oResult;
        if (this.Complete()) {
            global.user = new User(this.user.ID, this.user.email, this.user.FirstName, this.user.LastName, this.user.Active.byteValue() == 1,
                    this.user.ProfessionalMode.byteValue() == 1, false, this.user.IsBlocked.byteValue() == 1, 0.0, 0.0, 0, this.user.Token);      //default on creation is for location to not be displayed on the map
            preferenceManager.writeStringPreference(TFSPreferenceManager.Preferences.password, request.password);
        }
        super.Done(i);
    }

    public class Request {
        String firstName;
        String lastName;
        String email;
        String password;
    }

    class RegisterUser {
        public Integer ID;
        public String email;
        public String FirstName;
        public String LastName;
        public Number Active;
        public Number IsBlocked;
        public Number ProfessionalMode;
        public String Token;
    }

}

