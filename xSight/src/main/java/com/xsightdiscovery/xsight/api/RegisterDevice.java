package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Devices;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

/**
 * Created by rbrinkman on 6/26/14.
 */
@Singleton
public class RegisterDevice extends TFSInterface {

    @Inject Global global;

    private static String baseUrl = "https://xsight.azure-mobile.net/tables/Devices";
    public Object result;

    @Inject public RegisterDevice() {
        super();
        super.InitPost(baseUrl, Object.class);
    }

    public void Init(Devices deviceToRegister) {
        oRequest = deviceToRegister;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = oResult;
        super.Done(i);
    }
}
