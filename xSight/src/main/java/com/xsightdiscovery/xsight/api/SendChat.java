package com.xsightdiscovery.xsight.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;
import com.xsightdiscovery.xsight.api.Models.Chat;

import java.text.SimpleDateFormat;
import java.util.HashMap;

public class SendChat extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/chats";
    public Request request;
    public Chat result;


    @Inject public SendChat() {
        super();
        super.InitPost(myUrl, Chat.class);
    }

    public void Init(int toUserID, String message) {
        request = new Request();
        request.toUserID = toUserID;
        request.message = message;
        oRequest = request;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (Chat)oResult;
        result.Clean();
        super.Done(i);
    }

    public class Request {
        public Integer toUserID;
        public String message;
    }
}

