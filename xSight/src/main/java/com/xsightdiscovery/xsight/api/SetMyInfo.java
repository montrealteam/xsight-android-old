package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class SetMyInfo extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/users/";
    public Request request;
    public Result result;

    @Inject public SetMyInfo() {
        super();
        super.InitPost(myUrl, Result.class);
    }

    public void Init() {
        request = new Request();
        request.email = global.user.Email;
        request.firstName = global.user.FirstName;
        request.lastName = global.user.LastName;
        request.active = global.user.Active;
        request.professionalMode = global.user.ProfessionalMode;
        request.facebookPicUrl = global.user.FacebookPicUrl;
        request.linkedInPicUrl = global.user.LinkedInPicUrl;
        request.twitterPicUrl = global.user.TwitterPicUrl;
        request.isMapVisible = global.user.IsMapVisible;
        request.latitude = global.user.Latitude;
        request.longitude = global.user.Longitude;
        oRequest = request;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);
        headers.put("X-HTTP-Method-Override", "PATCH");

        super.Execute(done, global.user.ID.toString());
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (Result)oResult;
        if (result != null) {
            global.user.Email = result.Email;
            global.user.FirstName = result.FirstName;
            global.user.LastName = result.LastName;
            global.user.Active = result.Active;
            global.user.ProfessionalMode = result.ProfessionalMode;
            global.user.FacebookPicUrl = result.FacebookPicUrl;
            global.user.LinkedInPicUrl = result.LinkedInPicUrl;
            global.user.TwitterPicUrl = result.TwitterPicUrl;
            global.user.IsMapVisible = result.IsMapVisible;
            global.user.Latitude = result.Latitude;
            global.user.Longitude = result.Longitude;
        }
        super.Done(i);
    }


    public class Request {
        public String email;
        public String firstName;
        public String lastName;
        public Boolean active;
        public Boolean professionalMode;
        public String facebookPicUrl;
        public String linkedInPicUrl;
        public String twitterPicUrl;
        public Boolean isMapVisible;
        public Double latitude;
        public Double longitude;
    }


    public class Result {
        public Integer ID;
        public String Email;
        public String FirstName;
        public String LastName;
        public Boolean Active;
        public Boolean ProfessionalMode;
        public String FacebookPicUrl;
        public String LinkedInPicUrl;
        public String TwitterPicUrl;
        public Boolean IsMapVisible;
        public Double Latitude;
        public Double Longitude;
    }

}

