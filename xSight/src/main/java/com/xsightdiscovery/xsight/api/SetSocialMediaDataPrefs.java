package com.xsightdiscovery.xsight.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class SetSocialMediaDataPrefs extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/api/socialmediadatatypepreferences";
    private final Gson gson = new Gson();
    public Request[] request;
    public Result[] result;

    @Inject public SetSocialMediaDataPrefs() {
        super();
        super.InitPost(myUrl, Result[].class);
    }

    public void Init() {
        oRequest = null;
    }

    public void Init(GetSocialMediaDataPrefs.SocialMediaDataPrefs[] prefs ) {
        request = new Request[prefs.length];
        for (int i = 0; i <prefs.length; i++) {
            request[i] = new Request();
            request[i].typeID = prefs[i].Type.ID;
            request[i].share = prefs[i].Share == 1;
        }
        oRequest = request;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (Result[])oResult;
        super.Done(i);
    }

    public class Request {
        public Integer typeID;
        public boolean share;
    }

    public class Result {
        public String result;
        public ResultItem item;
        public String error;
    }

    public class ResultItem {
        public Integer userID;
        public Integer typeID;
        public Boolean share;
    }

}

