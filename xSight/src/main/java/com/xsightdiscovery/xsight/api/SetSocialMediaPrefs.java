package com.xsightdiscovery.xsight.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class SetSocialMediaPrefs extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/tables/socialmediapreferences";
    private final Gson gson = new Gson();
    public Request request;
    public SocialMediaPrefs result;

    @Inject public SetSocialMediaPrefs() {
        super();
        super.InitPost(myUrl, SocialMediaPrefs.class);
    }

    public void Init() {
        oRequest = null;
    }

    public void Init(int socialMediumID, boolean professionalMode, boolean personalMode) {
        request = new Request();
        request.socialMediumID = socialMediumID;
        request.professionalProfile = professionalMode;
        request.personalProfile = personalMode;
        oRequest = request;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (SocialMediaPrefs)oResult;
        super.Done(i);
    }

    public class Request {
        public Integer socialMediumID;
        public Boolean professionalProfile;
        public Boolean personalProfile;
    }

    public class SocialMediaPrefs {
        public Integer ID;
        public Integer UserID;
        public Integer SocialMediumID;
        public Boolean ProfessionalProfile;
        public Boolean PersonalProfile;
    }

}

