package com.xsightdiscovery.xsight.api;

import com.google.inject.Inject;
import com.truefitsolutions.core.network.TFSInterface;
import com.xsightdiscovery.xsight.Global;

import java.util.HashMap;

public class SetSocialMediaProfile extends TFSInterface {

    @Inject Global global;

    private static String myUrl = "https://xsight.azure-mobile.net/api/fullprofile";
    public Request[] request;
    public Result[] result;

    @Inject public SetSocialMediaProfile() {
        super();
        super.InitPost(myUrl, Result[].class);
    }

    public void Init() {
        oRequest = null;
    }

    public void Init(SetSocialMediaProfile.Request[] profileItems ) {
        oRequest = profileItems;
    }

    @Override
    public void Execute(Done done){
        headers = new HashMap<String, String>();
        headers.put("x-zumo-auth", global.user.Token);

        super.Execute(done);
    }

    @Override
    protected void Done(TFSInterface i) {
        result = (Result[])oResult;
        super.Done(i);
    }

    public static class Request {
        public Integer typeID;
        public String value;
        public String startDate;
        public String endDate;
        public String customID;
    }

    public class Result {
        public String result;
        public ResultItem item;
        public String error;
    }

    public class ResultItem {
        public Integer typeID;
        public String value;
        public String StartDate;
        public String EndDate;
        public String CustomID;
        public Integer userID;
        public Integer id;
    }

}

