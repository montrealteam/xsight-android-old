package com.xsightdiscovery.xsight.api;

import com.truefitsolutions.core.social.DialogListener;
import com.truefitsolutions.core.social.ExtendedProfile;
import com.truefitsolutions.core.social.LinkedinProfile;
import com.truefitsolutions.core.social.SocialAuthAdapter;
import com.truefitsolutions.core.social.SocialAuthError;
import com.truefitsolutions.core.social.SocialAuthListener;
import com.truefitsolutions.core.social.Tweet;
import com.xsightdiscovery.xsight.Global;

import org.brickred.socialauth.Contact;

import java.security.AuthProvider;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class XSightSocialMediaAdapter extends SocialAuthAdapter {

    public abstract static interface SocialMediaRetrievalDelegate {
        public abstract void socialMediaDataRetrieved(List<SetSocialMediaProfile.Request> profile);
    }

    DialogListener listener;
    SocialMediaRetrievalDelegate delegate;

    Integer saveAfter = 0;
    List<SetSocialMediaProfile.Request> profile;
    Global global;

    public XSightSocialMediaAdapter(DialogListener listener, Global global) {
        super(listener);
        this.listener = listener;
        this.global = global;
        profile = new LinkedList<SetSocialMediaProfile.Request>();
    }

    public XSightSocialMediaAdapter(SocialMediaRetrievalDelegate delegate, Global global) {
        this.delegate = delegate;
        this.global = global;
        profile = new LinkedList<SetSocialMediaProfile.Request>();
    }

    public void GetAndSaveSocialMediaProfile() {
        profile.clear();

        String pictureUrl = "";
        try {
            if (global.user.FirstName == "") {
                global.user.FirstName = getCurrentProvider().getUserProfile().getFirstName();
            }
            if (global.user.LastName == "") {
                global.user.LastName = getCurrentProvider().getUserProfile().getLastName();
            }
            pictureUrl = getCurrentProvider().getUserProfile().getProfileImageURL();
        } catch (Exception e) {
                e.printStackTrace();
        }

        org.brickred.socialauth.AuthProvider authProvider = getCurrentProvider();
        if(authProvider == null)
            return;

        String providerId = authProvider.getProviderId();

        if (providerId != null && providerId.equalsIgnoreCase("facebook")) {
            saveAfter = 1;
            global.user.FacebookPicUrl = pictureUrl + "?type=large";
            getExtendedUserProfileAsync(new ExtendedProfileDataListener());
            return;
        }
        if (providerId != null && providerId.equalsIgnoreCase("twitter")) {
            saveAfter = 2;
            global.user.TwitterPicUrl = pictureUrl;
            getTweetsAsync(new TweetsDataListener());
            getContactListAsync(new ContactsDataListener());
            return;
        }
        if (providerId != null && providerId.equalsIgnoreCase("linkedin")) {
            saveAfter = 1;
            global.user.LinkedInPicUrl = pictureUrl;
            getLinkedinUserProfileAsync(new LinkedinProfileDataListener());
            return;
        }

        if (listener != null) {
            listener.onAllDone(null);
        }

        return;
    }

    private void AddToProfile(String value, Integer type, String customID, String startDate, String endDate  ) {
        if (value != null) {
            SetSocialMediaProfile.Request item = new SetSocialMediaProfile.Request();
            item.typeID = type;
            item.customID = customID;
            item.value = value;
            item.startDate = startDate;
            item.endDate = endDate;
            profile.add(item);
        }
    }

    private void AddArrayToProfile(ExtendedProfile.FacebookValue[] array, Integer type) {
        if (array != null) {
            for (ExtendedProfile.FacebookValue v : array) {
                AddToProfile(v.name, type, v.id.toString(), null, null);
            }
        }
    }

    private void AddArrayInsideDataToProfile(ExtendedProfile.FacebookValueInsideData data, Integer type) {
        if (data != null) {
            AddArrayToProfile(data.data, type);
        }
    }

    private final class ExtendedProfileDataListener implements SocialAuthListener<ExtendedProfile> {

        @Override
        public void onExecute(String provider, ExtendedProfile t) {

            AddToProfile(t.bio, 4, null, null, null);
            AddToProfile(t.birthday, 5, null, null, null);
            AddArrayToProfile(t.checkins, 6);

            if(t.education != null) {
                for(int index = 0; index < t.education.length; index++) {
                    String education = t.education[index].school.name;
                    if (t.education[index].degree != null)
                        education += " for " + t.education[index].degree;
                    if (t.education[index].year != null)
                        education += "(" + t.education[index].year.name + ")";
                    AddToProfile(education, 7, null, null, null);
                }
            }
            AddArrayToProfile(t.events, 8);
            AddArrayToProfile(t.groups, 9);
            if(t.hometown != null)
                AddToProfile(t.hometown.name, 10, t.hometown.id.toString(), null, null);
            AddArrayToProfile(t.likes, 11);
            AddToProfile(t.religion, 12, null, null, null);

            if(t.work != null) {
                for(int index = 0; index < t.work.length; index++) {
                    String work = t.work[index].employer.name;
                    if (t.work[index].position != null && t.work[index].position.name != "")
                        work += " as a " + t.work[index].position.name;
                    if (t.work[index].location != null)
                        work += " in " + t.work[index].location;
                    if (t.work[index].end_date != null)
                        work += "(" + t.work[index].start_date + " - " + t.work[index].end_date + ")";
                    AddToProfile(work, 13, t.work[index].employer.id, null, null);
                }
            }
            String url = "http://graph.facebook.com/" + t.id + "/picture?type=large";
            AddToProfile(url, 14, null, null, null);
            AddToProfile(t.political, 30, null, null, null);

            AddArrayInsideDataToProfile(t.friends, 1005);
            AddArrayInsideDataToProfile(t.music, 1006);
            AddArrayInsideDataToProfile(t.movies, 1007);

            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }

        @Override
        public void onError(SocialAuthError e) {
            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }
    }

    private final class ContactsDataListener implements SocialAuthListener<List<Contact>> {

        @Override
        public void onExecute(String provider, List<Contact> contacts) {

            if (contacts != null) {
                for (Contact c : contacts) {
                    AddToProfile(c.getDisplayName(), 16, null, null, null);
                }
            }

            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }

        @Override
        public void onError(SocialAuthError e) {
            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }
    }

    private final class TweetsDataListener implements SocialAuthListener<List<Tweet>> {

        @Override
        public void onExecute(String provider, List<Tweet> tweets) {
            if (tweets != null) {
                for (Tweet t : tweets) {
                    AddToProfile(t.text, 15, null, null, null);
                }
            }

            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }

        @Override
        public void onError(SocialAuthError e) {
            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }
    }

    private final class LinkedinProfileDataListener implements SocialAuthListener<LinkedinProfile> {

        @Override
        public void onExecute(String provider, LinkedinProfile t) {

            AddToProfile(t.headline, 21, null, null, null);
            AddToProfile(t.industry, 22, null, null, null);
            if (t.location != null) {
                AddToProfile(t.location.name, 24, null, null, null);
            }
            if (t.connections != null && t.connections.values != null) {
                for(int index = 0; index < t.connections.values.length; index++) {
                    String name = t.connections.values[index].firstName + " " + t.connections.values[index].lastName;
                    AddToProfile(name, 17, t.connections.values[index].id, null, null);
                }
            }
            if (t.educations != null && t.educations.values != null) {
                for(int index = 0; index < t.educations.values.length; index++) {
                    LinkedinProfile.Education education = t.educations.values[index];
                    String formatEducation = String.format("%s in %s from %s (%s - %s)",education.degree,education.fieldOfStudy,education.schoolName,education.startDate.year,(education.endDate.year != null ? education.endDate.year : "current"));
                    AddToProfile(formatEducation, 20, education.id, null, null);
                }
            }
            if (t.following != null && t.following.companies != null && t.following.companies.values != null) {
                for(int index = 0; index < t.following.companies.values.length; index++) {
                    AddToProfile(t.following.companies.values[index].name, 18, t.following.companies.values[index].id, null, null);
                }
            }
//            for(int index = 0; index < t.groupMemberships.values.length; index++) {
//                AddToProfile(t.following.companies.values[index].name, 19, t.following.companies.values[index].id, null, null);
//            }
            if (t.skills != null && t.skills.values != null) {
                for(int index = 0; index < t.skills.values.length; index++) {
                    AddToProfile(t.skills.values[index].skill.name, 26, t.skills.values[index].id, null, null);
                }
            }
            if (t.threeCurrentPositions != null && t.threeCurrentPositions.values != null) {
                for(int index = 0; index < t.threeCurrentPositions.values.length; index++) {
                    AddToProfile(t.threeCurrentPositions.values[index].company.name, 27, t.threeCurrentPositions.values[index].company.id, null, null);
                }
            }
            if (t.threePastPositions != null && t.threePastPositions.values != null) {
                for(int index = 0; index < t.threePastPositions.values.length; index++) {
                    AddToProfile(t.threePastPositions.values[index].company.name, 28, t.threePastPositions.values[index].company.id, null, null);
                }
            }
            AddToProfile(t.pictureUrl, 29, null, null, null);

            if (global.user.FirstName == "") {
                global.user.FirstName = t.firstName;
            }
            if (global.user.LastName == "") {
                global.user.LastName = t.lastName;
            }
            global.user.LinkedInPicUrl = t.pictureUrl;

//
//-(void)map:(ApiLinkedinResult*)source into:(ApiSMContainer*)target {
//
//
//        for (ApiLinkedinObject* membership in source.groupMemberships.values) {
//        [items addObject:[ApiSMItem objectWith:@{CLASS_KEYPATH(ApiSMItem, typeID): @19,
//        CLASS_KEYPATH(ApiSMItem, value): membership.group.name,
//        CLASS_KEYPATH(ApiSMItem, customID): membership.group.id
//        }]];
//        }
//


            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }

        @Override
        public void onError(SocialAuthError e) {
            saveAfter--;
            if (saveAfter == 0)
            {
                saveSocialMediaProfile();
            }
        }
    }

    private void saveSocialMediaProfile() {

        if (profile != null) {
            SetSocialMediaProfile.Request[] profileArray = profile.toArray(new SetSocialMediaProfile.Request[profile.size()]);
            HashMap<String, SetSocialMediaProfile.Request> uniqueProfileHashmap = new HashMap<String, SetSocialMediaProfile.Request>();
            LinkedList<SetSocialMediaProfile.Request> uniqueProfileList = new LinkedList<SetSocialMediaProfile.Request>();

            for(SetSocialMediaProfile.Request profileItem : profileArray) {
                String key = profileItem.customID;

                if(key == null)
                    key = UUID.randomUUID().toString();

                if(!uniqueProfileHashmap.containsKey(key)) {
                    uniqueProfileHashmap.put(key, profileItem);
                    uniqueProfileList.add(profileItem);
                }
            }

            if(listener != null)
                listener.onAllDone(uniqueProfileList);
            if(delegate != null)
                delegate.socialMediaDataRetrieved(uniqueProfileList);
        }
    }
}
