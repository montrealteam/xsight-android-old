package com.xsightdiscovery.xsight;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.truefitsolutions.core.TFSFragmentActivity;

import roboguice.inject.InjectView;

public class xSightActivity extends TFSFragmentActivity {

    @InjectView(R.id.ab_facebook) View ab_facebook;
    @InjectView(R.id.ab_linkedIn) View ab_linkedIn;
    @InjectView(R.id.ab_twitter) View ab_twitter;
    @InjectView(R.id.ab_title) TextView ab_title;
    View homeIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        flurryResID = R.string.flurry_api_key;
        super.onCreate(savedInstanceState);
        forceOverflowMenu();

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        LayoutInflater li = LayoutInflater.from(this);
        View customView = li.inflate(R.layout.actionbar, null);
        actionBar.setCustomView(customView);

        actionBar.setDisplayHomeAsUpEnabled(true);
        homeIcon = findViewById(android.R.id.home);
        ((View) homeIcon.getParent()).setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
 }
